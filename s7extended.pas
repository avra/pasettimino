////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                            //
//  PASETTIMINO - S7EXTENDED - Extended library for direct communication to Siemens Simatic S7 PLC family     //
//                                                                                                            //
//  S7Extended lib expands original Pasettimino S7Client class with ability to read status of all PLC leds.   //
//  This means that you can read what leds are supported by your plc and read their status, while maintaning  //
//  full compatibility with original S7Client (GetPlcStatus still returns the same started/stopped/unknown    //
//  status as before), while reading all leds in the same PLC call so you can use this new info whenever you  //
//  want. Leds info can be found in new Leds[] record, or by using new methods to retreive the status of PLC, //
//  CPUs, and individual leds. Original S7Client reported incorrect run/stop status on redundant S7-400H      //
//  systems, while S7Extended reports correct status on all PLCs tested so far (S7-300, S7-400 and S7-400H).  //
//                                                                                                            //
//  Original Settimino for Arduino converted to Free Pascal by Zeljko Avramovic (user avra in Lazarus forum)  //
//                                                                                                            //
//  Original library documentation and license can be found at http://settimino.sourceforge.net               //
//  This source is published under the same license as original library (commercial use is allowed).          //
//  If you need more powerfull heavy weight library then you can take a look at http://snap7.sourceforge.net  //
//                                                                                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

unit s7extended;

{$mode objfpc}
{$modeswitch advancedrecords}
{$modeswitch typehelpers}

interface

uses
  Classes, SysUtils, strutils, dateutils,
  s7pas;

const
  // all led IDs (1..$15, $80..$84, $EC..$ED):
  S7_LED_SF_ID               = $01; // group error
  S7_LED_INTF_ID             = $02; // internal error
  S7_LED_EXTF_ID             = $03; // external error
  S7_LED_RUN_ID              = $04; // cpu in run mode
  S7_LED_STOP_ID             = $05; // cpu in stop mode
  S7_LED_FRCE_ID             = $06; // force
  S7_LED_CRST_ID             = $07; // cold restart
  S7_LED_BAF_ID              = $08; // battery fault/overload, short circuit of battery on bus
  S7_LED_USR_ID              = $09; // user defined
  S7_LED_USR1_ID             = $0A; // user defined
  S7_LED_BUS1F_ID            = $0B; // bus error interface 1
  S7_LED_BUS2F_ID            = $0C; // bus error interface 2 (or BUS5F - bus error, interface 5 with the CPUs 414-3 PN/DP, 416-3 PN/DP and 416F-3 PN/DP)
  S7_LED_REDF_ID             = $0D; // redundancy error
  S7_LED_MSTR_ID             = $0E; // master
  S7_LED_RACK0_ID            = $0F; // rack number 0
  S7_LED_RACK1_ID            = $10; // rack number 1
  S7_LED_RACK2_ID            = $11; // rack number 2
  S7_LED_IFM1F_ID            = $12; // interface error interface module 1
  S7_LED_IFM2F_ID            = $13; // interface error interface module 2
  S7_LED_BUS3F_ID            = $14; // bus error interface 3
  S7_LED_MAINT_ID            = $15; // maintenance demand
  S7_LED_DC24V_ID            = $16; // power supply
  //
  S7_LED_IF_ID               = $80; // init failure
  S7_LED_UF_ID               = $81; // user failure
  S7_LED_MF_ID               = $82; // monitoring failure
  S7_LED_CF_ID               = $83; // communication failure
  S7_LED_TF_ID               = $84; // task falure
  //
  S7_LED_APPL_STATE_RED_ID   = $EC; // ?
  S7_LED_APPL_STATE_GREEN_ID = $ED; // ?
  //
  S7_TOTAL_NUMBER_OF_LEDS    = $16 + 5 + 2;

  S7_LED_STATUS_LED_OFF = 0;
  S7_LED_STATUS_LED_ON  = 1;

  S7_FLASH_STATUS_NO_FLASHING   = 0; // 0.0 Hz
  S7_FLASH_STATUS_FAST_FLASHING = 1; // 2.0 Hz
  S7_FLASH_STATUS_SLOW_FLASHING = 2; // 0.5 Hz

  ERR_S7_LED_NOT_FOUND = 0;

  // all led TXTs (1..$15, $80..$84, $EC..$ED):
  S7_LED_SF_TXT               = 'SF';    // group error
  S7_LED_INTF_TXT             = 'INTF';  // internal error
  S7_LED_EXTF_TXT             = 'EXTF';  // external error
  S7_LED_RUN_TXT              = 'RUN';   // cpu in run mode
  S7_LED_STOP_TXT             = 'STOP';  // cpu in stop mode
  S7_LED_FRCE_TXT             = 'FRCE';  // force
  S7_LED_CRST_TXT             = 'CRST';  // cold restart
  S7_LED_BAF_TXT              = 'BAF';   // battery fault/overload, short circuit of battery on bus
  S7_LED_USR_TXT              = 'USR';   // user defined
  S7_LED_USR1_TXT             = 'USR1';  // user defined
  S7_LED_BUS1F_TXT            = 'BUS1F'; // bus error interface 1
  S7_LED_BUS2F_TXT            = 'BUS2F'; // bus error interface 2 (or BUS5F - bus error, interface 5 with the CPUs 414-3 PN/DP, 416-3 PN/DP and 416F-3 PN/DP)
  S7_LED_REDF_TXT             = 'REDF';  // redundancy error
  S7_LED_MSTR_TXT             = 'MSTR';  // master
  S7_LED_RACK0_TXT            = 'RACK0'; // rack number 0
  S7_LED_RACK1_TXT            = 'RACK1'; // rack number 1
  S7_LED_RACK2_TXT            = 'RACK2'; // rack number 2
  S7_LED_IFM1F_TXT            = 'IFM1F'; // interface error interface module 1
  S7_LED_IFM2F_TXT            = 'IFM2F'; // interface error interface module 2
  S7_LED_BUS3F_TXT            = 'BUS3F'; // bus error interface 3
  S7_LED_MAINT_TXT            = 'MAINT'; // maintenance demand
  S7_LED_DC24V_TXT            = 'DC24V'; // power supply
  //
  S7_LED_IF_TXT               = 'IF';    // init failure
  S7_LED_UF_TXT               = 'UF';    // user failure
  S7_LED_MF_TXT               = 'MF';    // monitoring failure
  S7_LED_CF_TXT               = 'CF';    // communication failure
  S7_LED_TF_TXT               = 'TF';    // task falure
  //
  S7_LED_APPL_STATE_RED_TXT   = 'APPL_STATE_RED';   // ?
  S7_LED_APPL_STATE_GREEN_TXT = 'APPL_STATE_GREEN'; // ?

  // PDU Type constants - ISO 8073, not all are mentioned in RFC 1006
  // For our purposes we use only those labeled with **
  // These constants contains 4 low bit order 0 (credit nibble)
  S7_PDU_TYPE_ED = $10; //     $10 ED : Expedited Data
  S7_PDU_TYPE_EA = $20; //     $20 EA : Expedited Data Ack
  S7_PDU_TYPE_UD = $40; //     $40 UD : CLTP UD
  S7_PDU_TYPE_RJ = $50; //     $50 RJ : Reject
  S7_PDU_TYPE_AK = $70; //     $70 AK : Ack data
  S7_PDU_TYPE_DR = $80; // **  $80 DR : Disconnect request (note : S7 doesn't use it)
  S7_PDU_TYPE_DC = $C0; // **  $C0 DC : Disconnect confirm (note : S7 doesn't use it)
  S7_PDU_TYPE_CC = $D0; // **  $D0 CC : Connection confirm
  S7_PDU_TYPE_CR = $E0; // **  $E0 CR : Connection request
  S7_PDU_TYPE_DT = $F0; // **  $F0 DT : Data transfer

  S7_MAX_VARS = 20; // maximal number of variables in iso data

  // CLI error codes which I have changed to fit into word
  errNegotiatingPDU            = $1000;
  //
  errCliInvalidParams          = $7F00; // non severe errors
  errCliJobPending             = $7E00;
  errCliTooManyItems           = $7D00;
  errCliInvalidWordLen         = $7C00;
  errCliPartialDataWritten     = $7B00;
  errCliSizeOverPDU            = $7A00;
  errCliInvalidPlcAnswer       = $7900;
  errCliAddressOutOfRange      = $7800;
  errCliInvalidTransportSize   = $7700;
  errCliWriteDataSizeMismatch  = $7600;
  errCliItemNotAvailable       = $7500;
  errCliInvalidValue           = $7400;
  errCliCannotStartPLC         = $7300;
  errCliAlreadyRun             = $7200;
  errCliCannotStopPLC          = $7100;
  errCliCannotCopyRamToRom     = $7000;
  errCliCannotCompress         = $6F00;
  errCliAlreadyStop            = $6E00;
  errCliFunNotAvailable        = $6D00;
  errCliUploadSequenceFailed   = $6C00;
  errCliInvalidDataSizeRecvd   = $6B00;
  errCliInvalidBlockType       = $6A00;
  errCliInvalidBlockNumber     = $6900;
  errCliInvalidBlockSize       = $6800;
  errCliDownloadSequenceFailed = $6700;
  errCliInsertRefused          = $6600;
  errCliDeleteRefused          = $6500;
  errCliNeedPassword           = $6400;
  errCliInvalidPassword        = $6300;
  errCliNoPasswordToSetOrClear = $6200;
  errCliJobTimeout             = $6100;
  errCliPartialDataRead        = $6000;
  errCliBufferTooSmall         = $5F00;
  errCliFunctionRefused        = $5E00;
  errCliDestroying             = $5D00;
  errCliInvalidParamNumber     = $5C00;
  errCliCannotChangeParam      = $5B00;

type

  //////////////////////////////////////////////////////////////////////////////////
  // ISO TCP PDU start
  //

  TTPKT = packed record   // TPKT Header - ISO on TCP - RFC 1006 (4 bytes)
    Version: byte;        // Always 3 for RFC 1006
    Reserved: byte;       // 0
    HI_Lenght: byte;      // High part of packet lenght (entire frame, payload and TPDU included)
    LO_Lenght: byte;      // Low part of packet lenght (entire frame, payload and TPDU included)
  end;                    // Packet length : min 7 max 65535
  PTPKT = ^TTPKT;

  TCOTP_DT = packed record
    HLength: byte;        // Header length : 3 for this header
    PDUType: byte;        // 0xF0 for this header
    EoT_Num: byte;        // EOT (bit 7) + PDU Number (bits 0..6)
                          // EOT = 1 -> End of Trasmission Packet (This packet is complete)
                          // PDU Number : Always 0
  end;
  PCOTP_DT = ^TCOTP_DT;

  TPacketInfo = packed record
    TPKT: TTPKT;
    COTP: TCOTP_DT;
    P: byte;
    PDUType: byte;        // S7_PDU_TYPE_ED. S7_PDU_TYPE_EA...
  end;
  PTPacketInfo = ^TPacketInfo;

  TS7ReqHeader = record
    P: byte;              // Telegram ID, always 32
    PDUType: byte;        // Header aType 1 or 7
    AB_EX: word;          // AB currently unknown, maybe it can be used for long numbers.
    Sequence: word;       // Message ID. This can be used to make sure a received answer
    ParLen: word;         // Length of parameters which follow this header
    DataLen: word;        // Length of data which follow the parameters
  end;
  PS7ReqHeader = ^TS7ReqHeader;

const
  // ISO_PAYLOAD_SIZE = SizeOf(TPDU) - SizeOF(TTPKT) - SizeOf(TCOTP_DT);  // Iso telegram Buffer size (4096 in Snap7, but I want PDU[] to use Pasettimino later on 8 bit AVR so we keep it small here)
  ISO_PAYLOAD_SIZE  = 4096;
  ISO_PAYLOAD_ITEMS = ((ISO_PAYLOAD_SIZE - 29) div 4) - 1; // Note : 29 is the size of headers iso, COPT, S7 header, params, data

type
  TIsoPayLoad = packed array [0..ISO_PAYLOAD_SIZE] of byte; // in Snap7 this goes to 4096 but our PDU[] in s7pas is much smaller
  PIsoPayload = ^TIsoPayload;

  TIsoDataPDU = record
    TPKT: TTPKT;          // TPKT Header
    COTP: TCOTP_DT;       // COPT Header for DATA EXCHANGE
    Payload: TIsoPayload; // Payload
  end;
  PIsoDataPDU = ^TIsoDataPDU;

  {============================================================================== }
  {                            GROUP BLOCKS INFO }
  {============================================================================== }

  TReqFunTypedParams = packed record  // Most used request type parameters record
    Head: packed array[0..2] of byte; // 0x00 0x01 0x12
    Plen: byte;                       // par len 0x04
    Uk: byte;                         // unknown
    Tg: byte;                         // type and group  (4 bits type and 4 bits group)
    SubFun: byte;                     // subfunction
    Seq: byte;                        // sequence
  end;
  PTReqFunTypedParams = ^TReqFunTypedParams;

  PTReqFunGetBlockInfo = ^TReqFunGetBlockInfo;
  TReqFunGetBlockInfo = TReqFunTypedParams;

  TDummyReqParams = packed record
    dummy: packed array[0..26] of byte;
    ReqParams: TReqFunTypedParams;
  end;

  TReqDataBlockOfType = packed record
    RetVal: byte;  // 0xFF
    TSize: byte;   // Octet (0x09)
    Length: word;  // 0x0002
    Zero: byte;    // Ascii '0' (0x30)
    BlkType: byte;
  end;
  PTReqDataBlockOfType = ^TReqDataBlockOfType;

  TResFunGetBlockInfo = packed record
      Head: packed array[0..2] of byte;
      Plen: byte;
      Uk: byte;
      Tg: byte;
      SubFun: byte;
      Seq: byte;
      Rsvd: word;
      ErrNo: word;
    end;
  PTResFunGetBlockInfo = ^TResFunGetBlockInfo;

  TDataFunGetBotItem = packed record
    BlockNum: word;
    Unknown: byte;
    BlockLang: byte;
  end;
  PTDataFunGetBotItem = ^TDataFunGetBotItem;

  TDataFunGetBot = packed record
      RetVal: byte;
      TSize: byte;
      DataLen: word;
      Items: packed array[0..ISO_PAYLOAD_ITEMS] of TDataFunGetBotItem;
    end;
  PTDataFunGetBot = ^TDataFunGetBot;

  TDummyResData = packed record
    dummy: TResFunGetBlockInfo;
    ResData: TDataFunGetBot;
  end;

  {============================================================================== }
  {                               FUNCTION READ                                   }
  {============================================================================== }

  TReqFunReadItem = packed record
    ItemHead: packed array[0..2] of byte;
    TransportSize: byte;
    Length: word;
    DBNumber: word;
    Area: byte;
    Address: array[0..2] of byte;
  end;
  PTReqFunReadItem = ^TReqFunReadItem;

  TReqFunReadParams = packed record
    FunRead: byte;
    ItemsCount: byte;
    Items: packed array[0..S7_MAX_VARS-1] of TReqFunReadItem;
  end;
  PTReqFunReadParams = ^TReqFunReadParams;

  TResFunReadParams = packed record
    FunRead: byte;
    ItemCount: byte;
  end;
  PTResFunReadParams = ^TResFunReadParams;

  TResFunReadItem = packed record
    ReturnCode: byte;
    TransportSize: byte;
    DataLength: word;
    Data: packed array[0..ISO_PAYLOAD_SIZE-17-1] of byte; // 17 = header + params + data header - 1
  end;
  PTResFunReadItem = ^TResFunReadItem;

  TResFunReadData = packed array[0..S7_MAX_VARS-1] of PTResFunReadItem;

  TS7Buffer = packed array[0..65535] of byte;
  PTS7Buffer = ^TS7Buffer;

  TS7BlocksOfTypeWordsNum = 0..$1FFF;
  TS7BlocksOfType = packed array[TS7BlocksOfTypeWordsNum] of word;
  PTS7BlocksOfType = ^TS7BlocksOfType;

  {============================================================================== }
  {                               FUNCTION WRITE }
  {============================================================================== }

  TReqFunWriteItem = TReqFunReadItem; // read and write use the same structure
  PTReqFunWriteItem = ^TReqFunWriteItem;

  TReqFunWriteParams = packed record
    FunWrite: byte;
    ItemsCount: byte;
    Items: array[0..S7_MAX_VARS-1] of TReqFunWriteItem;
  end;
  PTReqFunWriteParams = ^TReqFunWriteParams;

  TReqFunWriteDataItem = TResFunReadItem; // read and write use the same structure
  PTReqFunWriteDataItem = ^TReqFunWriteDataItem;

  TReqFunWriteData = array[0..S7_MAX_VARS-1] of PTReqFunWriteDataItem;
  PTReqFunWriteData = ^TReqFunWriteData;

  TResFunWrite = packed record
    FunWrite: byte;
    ItemCount: byte;
    Data: array[0..S7_MAX_VARS-1] of byte;
  end;
  PTResFunWrite = ^TResFunWrite;

  //
  // ISO TCP PDU finish
  //////////////////////////////////////////////////////////////////////////////////


  TS7LedFromCpu        = (lfCpuMaster = -1, lfCpuZero = 0, lfCpuOne = 1); // from which CPU should led be read
  //
  TS7LedLightStatus    = S7_LED_STATUS_LED_OFF..S7_LED_STATUS_LED_ON;
  TS7LedFlashingStatus = S7_FLASH_STATUS_NO_FLASHING..S7_FLASH_STATUS_SLOW_FLASHING;
  TS7LedIdType         = byte; //led IDs can be 1..$15, $80..$84, $EC..$ED
  TS7RackNum           = 0..7; // bits 0..2
  TS7CpuNum            = 0..1;
  TS7LedIndex          = 1..S7_TOTAL_NUMBER_OF_LEDS;
  TS7LedIndexWithZero  = 0..S7_TOTAL_NUMBER_OF_LEDS; // 0 = error index

  TS7Cpu = bitpacked record // 1 byte, equals zero on standard CPUs (on H-System CPUs it is never zero)
    Rack: TS7RackNum;       // bits 0..2
    Master: boolean;        // bit 3
    Redundant: boolean;     // bit 4 (actually bits 4..7 are all ones (1111) in redundant H-System CPUs
  end;

  TS7Info = packed record
    case byte of
    0: (Cpu: TS7Cpu;
        LedID: byte;);
    1: (AsWord: word);
    2: (AsByte: array[0..1] of byte);
  end;

  TS7LedStatus = packed record
    case byte of
    0: (Status: TS7LedLightStatus);
    1: (AsByte: byte);
  end;

  TS7LedFlashing = packed record
    case byte of
    0: (Status: byte); //TS7LedFlashingStatus);
    1: (AsByte: byte);
  end;

  TS7Led = packed record         // single led info as received from SZL $0074 and $0174 (4 bytes)
    Found: boolean;              // does cpu have this led?
    procedure Init;
    function On: boolean;        // is led on?
    function Flashing: boolean;  // is led flashing?
    case byte of
    0: (Info: TS7Info;           // 1 word (cpu_led_ID)
        Led: TS7LedStatus;       // 1 byte (led_on)
        Flash: TS7LedFlashing);  // 1 byte (led_blink)
    1: (AsLongword: Longword);
    2: (AsByte: array[0..3] of byte);
  end;

  TS7LedArray = array [TS7LedIndex] of TS7Led;
  PS7LedArray = ^TS7LedArray;

  TS7CpuLeds = record
    Led: TS7LedArray;
    Master: boolean;
  end;

  TS7MasterCpuLeds = record
    Led: PS7LedArray; // points to Cpu[0].Led or Cpu[1].Led, whichever is the master
  end;

  TLedIDs   = array [TS7LedIndex] of TS7LedIdType;
  TLedNames = array [TS7LedIndex] of string;
  TLedCpus  = array [TS7CpuNum] of TS7CpuLeds;

  TS7Leds = record
    ID:   TLedIDs;
    Name: TLedNames;
    Cpu:  TLedCpus;
    MasterCpu: TS7MasterCpuLeds;
    MasterCpuNumber: TS7CpuNum;
    Redundant: boolean;
    NumberOfLeds: byte; // number of 4 byte data records
  end;

  TS7CpuInfo = packed record
    ProjectName    : packed array[0..24] of AnsiChar; // $0001
    ModuleName     : packed array[0..24] of AnsiChar; // $0002
    Copyright      : packed array[0..26] of AnsiChar; // $0004
    SerialNumber   : packed array[0..24] of AnsiChar; // $0005
    ModuleTypeName : packed array[0..32] of AnsiChar; // $0007
    Tmp: string;
  end;
  PS7CpuInfo = ^TS7CpuInfo;

  TS7CpInfo = packed record
    MaxPduLengt    : integer;
    MaxConnections : integer;
    MaxMpiRate     : integer;
    MaxBusRate     : integer;
  end;
  PS7CpInfo = ^TS7CpInfo;

  TS7OrderCode = packed record
    ProductNumber: packed array[0..20] of AnsiChar;
    V1           : byte;
    V2           : byte;
    V3           : byte;
    function Version(aWithLeadingVersionChar: boolean = false): string;
  end;
  PS7OrderCode = ^TS7OrderCode;

  TS7PlcInfo = packed record
    Cpu: TS7CpuInfo;
    Cp: TS7CpInfo;
    OrderCode: TS7OrderCode;
  end;

  TS7AreaMemoryType = (mtUnknown, mtRAM, mtFEPROM, mtRAMandFEPROM);

  TS7AreaMemoryTypeHelper = type helper for TS7AreaMemoryType
    function ToString: string;
  end;

  TS7AreaWithBytes = packed record
    Bytes: word;
    MemoryType: TS7AreaMemoryType;
    RetentiveElements: word;
  end;

  TS7AreaWithCounts = packed record
    Count: word;
    MemoryType: TS7AreaMemoryType;
    RetentiveElements: word;
  end;

  TS7SystemAreas = packed record // s7sfc_en-EN.pdf, Page 708:  SSL-ID W#16#xy14 - System Areas
    Inputs: TS7AreaWithBytes;
    Outputs: TS7AreaWithBytes;
    Memory: TS7AreaWithBytes;
    Timers: TS7AreaWithCounts;
    Counters: TS7AreaWithCounts;
    Logical: TS7AreaWithBytes;
    Local: TS7AreaWithBytes;
  end;

  TS7AreaInfo = packed record
    case byte of
    0: (AsByte:     array[0..7] of byte);
    1: (AsWord:     array[0..3] of word);
    2: (AsLongword: array[0..1] of longword);
    3: (AsQuadword: qword);
  end;
  PS7AreaInfo = ^TS7AreaInfo;

  TS7Block = packed record
    List: TS7BlocksOfType;
    Count: word;
  end;
  PTS7Block = ^TS7Block;

  TS7Blocks = packed record
    OB: TS7Block;
    DB: TS7Block;
    SDB: TS7Block;
    FC: TS7Block;
    SFC: TS7Block;
    FB: TS7Block;
    SFB: TS7Block;
  end;


  TS7MsgRequestHeaderRange = 0..32;
  TS7MsgRequestHeader      = array [TS7MsgRequestHeaderRange] of byte;

  TS7MsgRequestHeaderListBlocksOfRangeFirst = 0..30;
  TS7MsgRequestHeaderListBlocksOfFirst      = array [TS7MsgRequestHeaderListBlocksOfRangeFirst] of byte;

  TS7MsgRequestHeaderListBlocksOfNext = TS7MsgRequestHeader;

  TS7MsgRequestHeaderGetClockRange = 0..28;
  TS7MsgRequestHeaderGetClock      = array [TS7MsgRequestHeaderGetClockRange] of byte;

  TS7MsgRequestHeaderSetClockRange = 0..38;
  TS7MsgRequestHeaderSetClock      = array [TS7MsgRequestHeaderSetClockRange] of byte;

  TS7BlockID    = (blkOB = $38, blkDB = $41, blkSDB = $42, blkFC = $43, blkSFC = $44, blkFB = $45, blkSFB = $46);
  TS7BlockIDSet = set of TS7BlockID;

  TS7BlockIDHelper = type helper for TS7BlockID
    function ToString: string;
  end;

  TS7PowerSupplyBatteryStatus = (psbGood, psbFailure, psbNotSupported);

  TS7PowerSupplyBatteryStatusHelper = type helper for TS7PowerSupplyBatteryStatus
    function ToString: string;
  end;

const
  TS7AsciiChars: set of byte = [32..127]; // printable characters range in S7 messages

  LED_ID: TLedIDs     = (S7_LED_SF_ID,     S7_LED_INTF_ID,   S7_LED_EXTF_ID,  S7_LED_RUN_ID,   S7_LED_STOP_ID,  // $0001...
                         S7_LED_FRCE_ID,   S7_LED_CRST_ID,   S7_LED_BAF_ID,   S7_LED_USR_ID,   S7_LED_USR1_ID,  // ...
                         S7_LED_BUS1F_ID,  S7_LED_BUS2F_ID,  S7_LED_REDF_ID,  S7_LED_MSTR_ID,  S7_LED_RACK0_ID, // ...
                         S7_LED_RACK1_ID,  S7_LED_RACK2_ID,  S7_LED_IFM1F_ID, S7_LED_IFM2F_ID, S7_LED_BUS3F_ID, // ...
                         S7_LED_MAINT_ID,  S7_LED_DC24V_ID,                                                     // ...$0016
                         S7_LED_IF_ID,     S7_LED_UF_ID,     S7_LED_MF_ID,    S7_LED_CF_ID,    S7_LED_TF_ID,    // $0080..$0084
                         S7_LED_APPL_STATE_RED_ID,           S7_LED_APPL_STATE_GREEN_ID);                       // $00EC..$00ED

  LED_NAME: TLedNames = (S7_LED_SF_TXT,    S7_LED_INTF_TXT,  S7_LED_EXTF_TXT, S7_LED_RUN_TXT,   S7_LED_STOP_TXT,
                         S7_LED_FRCE_TXT,  S7_LED_CRST_TXT,  S7_LED_BAF_TXT,  S7_LED_USR_TXT,   S7_LED_USR1_TXT,
                         S7_LED_BUS1F_TXT, S7_LED_BUS2F_TXT, S7_LED_REDF_TXT, S7_LED_MSTR_TXT,  S7_LED_RACK0_TXT,
                         S7_LED_RACK1_TXT, S7_LED_RACK2_TXT, S7_LED_IFM1F_TXT,S7_LED_IFM2F_TXT, S7_LED_BUS3F_TXT,
                         S7_LED_MAINT_TXT, S7_LED_DC24V_TXT,
                         S7_LED_IF_TXT,    S7_LED_UF_TXT,    S7_LED_MF_TXT,   S7_LED_CF_TXT,    S7_LED_TF_TXT,
                         S7_LED_APPL_STATE_RED_TXT,          S7_LED_APPL_STATE_GREEN_TXT);

  // SZL ID $0074 for getting all leds info:
  S7_LEDS_SZL_ID                          = $0074;
  S7_LEDS_SZL_INDEX                       = $0000;
  //
  // SZL ID $001C for getting cpu info:
  S7_CPU_INFO_SZL_ID                      = $001C;
  S7_CPU_INFO_SZL_INDEX                   = $0000;
  //
  // SZL ID $0011 for getting identification order code (product number) and module version:
  S7_ORDER_CODE_SZL_ID                    = $0011;
  S7_ORDER_CODE_SZL_INDEX                 = $0000;
  //
  // SZL ID $0014 for getting system areas size:
  S7_SYSTEM_AREAS_SZL_ID                  = $0014;
  S7_SYSTEM_AREAS_SZL_INDEX               = $0000;
  //
  // SZL ID $0392 for getting power supply battery:
  S7_POWER_SUPPLY_BATTERY_FAULT_SZL_ID    = $0392;
  S7_POWER_SUPPLY_BATTERY_FAULT_SZL_INDEX = $0000;

  S7_MSG_REQ_HEADER: TS7MsgRequestHeader = (  // S7 Get PLC LEDs Request Header (contains also ISO Header and COTP Header)
    $03, $00, $00, $21, $02, $f0, $80, $32,
    $07, $00, $00, $2c, $00, $00, $08, $00,
    $08, $00, $01, $12, $04, $11, $44, $01,
    $00, $ff, $09, $00, $04, $00, $74, $00,   // SZL ID: default is $0074 for getting all leds info
    $00
  );

  S7_MSG_REQ_HEADER_LIST_BLOCKS_OF_FIRST: TS7MsgRequestHeaderListBlocksOfFirst = (  // Req Header (contains also ISO Header and COTP Header)
    $03, $00, $00, $1f, $02, $f0, $80, $32,
    $07, $00, $00, $05, $00, $00, $08, $00,
    $06, $00, $01, $12, $04, $11, $43, $02,
    $00, $ff, $09, $00, $02, $30, $41
  );

  S7_MSG_REQ_HEADER_LIST_BLOCKS_OF_NEXT: TS7MsgRequestHeaderListBlocksOfNext = (  // Req Header (contains also ISO Header and COTP Header)
    $03, $00, $00, $21, $02, $f0, $80, $32,
    $07, $00, $00, $25, $00, $00, $0c, $00,
    $04, $00, $01, $12, $08, $12, $43, $02,
    $02, $00, $00, $00, $00, $0a, $00, $00,
    $00
  );

  S7_MSG_REQ_HEADER_GET_TIME: TS7MsgRequestHeaderGetClock = (  // Req Header (contains also ISO Header and COTP Header)
    $03, $00, $00, $1d, $02, $f0, $80, $32,
    $07, $00, $00, $08, $00, $00, $08, $00,
    $04, $00, $01, $12, $04, $11, $47, $01,
    $00, $0a, $00, $00, $00
  );

  S7_MSG_REQ_HEADER_SET_TIME: TS7MsgRequestHeaderSetClock = (  // Req Header (contains also ISO Header and COTP Header)
    $03, $00, $00, $27, $02, $f0, $80, $32,
    $07, $00, $00, $da, $09, $00, $08, $00,
    $0e, $00, $01, $12, $04, $11, $47, $02,
    $00, $ff, $09, $00, $0a, $00, $19, $18,
    $08, $10, $14, $51, $45, $00, $06
  );

  S7BlockIDSet: TS7BlockIDSet = [blkOB, blkDB, blkSDB, blkFC, blkSFC, blkFB, blkSFB]; // needed to iterate all blocks in for loop (like "for BlockID in S7BlockIDSet do")

type

  { TS7ExtendedClient }

  TS7ExtendedClient = class(TS7Client)
  private
    FReplyMsgLength: word;
    procedure Init;
    procedure InitLeds;
    procedure InitCpuInfo;
    procedure InitOrderCode;
    procedure InitAreas;
    procedure InitBlocks;
    procedure InitBatteryStatus;
    procedure InitClock;
    procedure SetSZL(const aId, aIndex: word);
    function  GetMemoryType(const aMemType: word): TS7AreaMemoryType;
    function  GetLastReceivedMessageLength: word;
  public
    Leds: TS7Leds;
    Info: TS7PlcInfo;
    Area: TS7SystemAreas;
    Blocks: TS7Blocks;
    Battery: TS7PowerSupplyBatteryStatus;
    Time: TDateTime;
    constructor Create;
    function GetLedIndex(const aLedID: TS7LedIdType): TS7LedIndexWithZero;
    function GetLedIndex(const aLedName: string): TS7LedIndexWithZero;
    function GetLedID(const aLedIndex: TS7LedIndex): TS7LedIdType;
    function GetLedID(const aLedName: string): TS7LedIdType;
    function GetLedName(const aLedID: TS7LedIdType): string;
    function Led(const aLedID: TS7LedIdType; aCpuNum: TS7LedFromCpu = lfCpuMaster): TS7Led;
    function Led(const aLedName: string; aCpuNum: TS7LedFromCpu = lfCpuMaster): TS7Led;
    function LedOn(const aLedID: TS7LedIdType; aCpuNum: TS7LedFromCpu = lfCpuMaster): boolean;
    function LedOn(const aLedName: string; aCpuNum: TS7LedFromCpu = lfCpuMaster): boolean;
    function GetPlcLeds: int;
    function GetPlcInfo: int;
    function GetPlcOrderCode: int;
    function GetPlcAreas: int;
    function GetPlcBatteryStatus: int;
    function GetBlockPointer(const aBlockID: TS7BlockID): PTS7Block;
    function ReadSZL(const aId, aIndex: word): int;
    function GetPlcBlockList(const aBlockID: TS7BlockID = blkDB): int;
    function GetPlcAllBlocksList: int;
    function GetPlcTime: int;
    function SetPlcTime(const aNewDateTime: TDateTime): int;
    function ErrorToString(aError: int): string;
    property LastReceivedMessageLength: word read GetLastReceivedMessageLength;
  end;

  function Dump(aBuffer: pbyte; const aLen: integer; const aNewLineCount: integer = High(integer)): ansistring;

var
  Sequence: word = 0; // autoinc for tcp iso messages
  // overlays below need detailed testing
  PduPacketInfo: TPacketInfo absolute PDU; { TODO : multithreading with s7comm not possible because absolute was used for PDU[] }
  // PduReqHeader: TS7ReqHeader absolute PduPacketInfo.P;
  PDUH_out: TS7ReqHeader absolute PduPacketInfo.P;
  PduIso: TIsoPayload absolute PDU;
  DummyReqParams: TDummyReqParams absolute PDU;
  ReqParams: TReqFunGetBlockInfo absolute DummyReqParams.ReqParams;
  ResParams: TResFunGetBlockInfo absolute DummyReqParams.ReqParams;
  DummyResData: TDummyResData absolute ResParams;
  ResData: TDataFunGetBot absolute DummyResData.ResData; // ResData:=PDataFunGetBot(pbyte(ResParams)+SizeOf(TResFunGetBlockInfo));

implementation


////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                            //
// Dump without aNewLineCount parameter:                                                      //
// 01 53 4E 41 50 37 2D 53 45 52 56 45 52 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  //
//                                                                                            //
// Dump with aNewLineCount parameter = 16:                                                    //
// 01 53 4E 41 50 37 2D 53 45 52 56 45 52 00 00 00 .SNAP7-SERVER...                           //
// 00 00 00 00 00 00 00 00 00 00 00 00 00 00       ..............                             //
//                                                                                            //
////////////////////////////////////////////////////////////////////////////////////////////////

function Dump(aBuffer: pbyte; const aLen: integer; const aNewLineCount: integer = High(integer)): ansistring;
var // dump buffer as hex string
  i: integer;
  buf: pbyte;
  s: string;
  cnt: byte = 0;
begin
  Result := '';
  s := '';

  if aBuffer = nil then
    aBuffer := @PDU.DATA;

  for i := 0 to aLen - 1 do
  begin
    buf := aBuffer + i;
    Result := Result + IntToHex(buf^, 2);
    if i <> aLen - 1 then
      Result := Result + ' ';

    if buf^ in [1..9] then
      Inc(cnt);

    if aNewLineCount <> High(integer) then
    begin
      if buf^ in TS7AsciiChars then
        s := s + char(buf^)
      else
        s := s + '.';

      if (((i - aNewLineCount + 1) mod aNewLineCount) = 0) or (i = aLen - 1) then
      begin
        if (Length(s) < aNewLineCount) or (i = aLen - 1) then
          Result := Result + Space((aNewLineCount - Length(s)) * 3) + ' '; // 3 = space + 2 hex chars
        Result := Result + '   ' + s;
        if i <> aLen - 1 then
          Result := Result + LineEnding;
        s := '';
      end;
    end;
  end;
end;

function GetNextSequence: word;
begin
  if Sequence = High(word) then
    Sequence := 1
  else
    Inc(Sequence);
  Result := {Swap}(Sequence); { TODO : Snap7 does not swap bytes in Sequence. Check if that is what Siemens does too. }
end;

function IntToBCD(value: int): int;
begin
  result :=                   value div 1000 mod 10;
  result := (result shl 4) or value div  100 mod 10;
  result := (result shl 4) or value div   10 mod 10;
  result := (result shl 4) or value          mod 10;
end;

///////////////////////////////////////////
//  TS7AreaMemoryTypeHelper type helper  //
///////////////////////////////////////////

function TS7AreaMemoryTypeHelper.ToString: string;
begin
  case Self of
    mtRAM:             Result := 'RAM';
    mtFEPROM:          Result := 'FEPROM';
    mtRAMandFEPROM:    Result := 'RAM+FEPROM';
  else
    {mtUnknown:}       Result := 'Unknown';
  end;
end;

//////////////////////////////
//  TS7BlockID type helper  //
//////////////////////////////

function TS7BlockIDHelper.ToString: string;
begin
  case Self of
    blkOB:    Result := 'OB';
    blkDB:    Result := 'DB';
    blkSDB:   Result := 'SDB';
    blkFC:    Result := 'FC';
    blkSFC:   Result := 'SFC';
    blkFB:    Result := 'FB';
    blkSFB:   Result := 'SFB';
  else
    Result := 'Unknown';
  end;
end;


///////////////////////////////////////////////
//  TS7PowerSupplyBatteryStatus type helper  //
///////////////////////////////////////////////

function TS7PowerSupplyBatteryStatusHelper.ToString: string;
begin
  case Self of
    psbGood:             Result := 'Battery is good';
    psbFailure:          Result := 'Battery failure';
  else
    {psbNotSupported:}   Result := 'Battery not supported';
  end;
end;


/////////////////////////////
//  TS7Led record helpers  //
/////////////////////////////

procedure TS7Led.Init;
begin
  Self.Found := false;
  Self.AsLongword := 0;
end;

function TS7Led.On: boolean;
begin
  Result := Self.Led.Status = S7_LED_STATUS_LED_ON;
end;

function TS7Led.Flashing: boolean;
begin
  Result := not (Self.Flash.Status = S7_FLASH_STATUS_NO_FLASHING);
end;


//////////////////////////////////
//  TS7OrderCode record helper  //
//////////////////////////////////

function TS7OrderCode.Version(aWithLeadingVersionChar: boolean = false): string;
begin
  if aWithLeadingVersionChar then
    Result := 'v'
  else
    Result := '';
  Result := Result + IntToStr(Self.V1) + '.' + IntToStr(Self.V2) + '.' + IntToStr(Self.V3);
end;


/////////////////////////
//  TS7ExtendedClient  //
/////////////////////////

constructor TS7ExtendedClient.Create;
begin
  inherited;
  Leds.ID   := LED_ID;
  Leds.Name := LED_NAME;
  Init;
end;

procedure TS7ExtendedClient.Init;
begin
  InitLeds;
  InitCpuInfo;
  InitOrderCode;
  InitAreas;
  InitBlocks;
  InitBatteryStatus;
  InitClock;
end;

procedure TS7ExtendedClient.InitLeds;
var
  CpuCnt, LedCnt: byte;
begin
  Leds.Redundant := false; // redundancy will be switched on only if found during reading leds
  Leds.NumberOfLeds := 0;
  Leds.MasterCpuNumber := 0;
  for CpuCnt := Low(Leds.Cpu) to High(Leds.Cpu) do
  begin
    Leds.Cpu[CpuCnt].Master := false;
    for LedCnt := Low(Leds.ID) to High(Leds.ID) do
    begin
      Leds.Cpu[CpuCnt].Led[LedCnt].Found      := false; // default for every led is that it does not exist in this cpu
      Leds.Cpu[CpuCnt].Led[LedCnt].AsLongword := 0;
    end;
  end;
end;

procedure TS7ExtendedClient.InitCpuInfo;
begin // init char arrays with zero, to make sure we will have nul terminated strings after copy
  FillChar(Info.Cpu.ProjectName, SizeOf(Info.Cpu.ProjectName), 0);
  FillChar(Info.Cpu.ModuleName, SizeOf(Info.Cpu.ModuleName), 0);
  FillChar(Info.Cpu.Copyright, SizeOf(Info.Cpu.Copyright), 0);
  FillChar(Info.Cpu.SerialNumber, SizeOf(Info.Cpu.SerialNumber), 0);
  FillChar(Info.Cpu.ModuleTypeName, SizeOf(Info.Cpu.ModuleTypeName), 0);
end;

procedure TS7ExtendedClient.InitOrderCode;
begin
  FillChar(Info.OrderCode.ProductNumber, SizeOf(Info.OrderCode.ProductNumber), 0);
  Info.OrderCode.V1 := 0;
  Info.OrderCode.V2 := 0;
  Info.OrderCode.V3 := 0;
end;

procedure TS7ExtendedClient.InitAreas;
begin
  //FillChar(Area, SizeOf(Area), 0);
  Area.Inputs.Bytes    := 0;    Area.Inputs.MemoryType   := mtUnknown;    Area.Inputs.RetentiveElements   := 0;
  Area.Outputs.Bytes   := 0;    Area.Outputs.MemoryType  := mtUnknown;    Area.Outputs.RetentiveElements  := 0;
  Area.Memory.Bytes    := 0;    Area.Memory.MemoryType   := mtUnknown;    Area.Memory.RetentiveElements   := 0;
  Area.Timers.Count    := 0;    Area.Timers.MemoryType   := mtUnknown;    Area.Timers.RetentiveElements   := 0;
  Area.Counters.Count  := 0;    Area.Counters.MemoryType := mtUnknown;    Area.Counters.RetentiveElements := 0;
  Area.Logical.Bytes   := 0;    Area.Logical.MemoryType  := mtUnknown;    Area.Logical.RetentiveElements  := 0;
  Area.Local.Bytes     := 0;    Area.Local.MemoryType    := mtUnknown;    Area.Local.RetentiveElements    := 0;
end;

procedure TS7ExtendedClient.InitBlocks;
begin
  FillChar(Blocks, SizeOf(Blocks), 0);
end;

procedure TS7ExtendedClient.InitBatteryStatus;
begin
  Battery := psbNotSupported;
end;

procedure TS7ExtendedClient.InitClock;
begin
  Time := EncodeDate(1900, 1, 1);
end;

function TS7ExtendedClient.GetLastReceivedMessageLength: word;
begin
  Result := FReplyMsgLength;
end;

function TS7ExtendedClient.GetLedID(const aLedIndex: TS7LedIndex): TS7LedIdType;
begin
  Result := LED_ID[aLedIndex];
end;

function TS7ExtendedClient.GetLedID(const aLedName: string): TS7LedIdType;
begin
  Result := LED_ID[GetLedIndex(aLedName)];
end;

function TS7ExtendedClient.GetLedIndex(const aLedID: TS7LedIdType): TS7LedIndexWithZero;
var
  i: byte;
begin
  Result := ERR_S7_LED_NOT_FOUND;
  for i := Low(LED_ID) to High(LED_ID) do
  begin
    if LED_ID[i] = aLedID then
    begin
      Result := i;
      Break;
    end;
  end;
end;

function TS7ExtendedClient.GetLedIndex(const aLedName: string): TS7LedIndexWithZero;
var
  i: byte;
begin
  Result := ERR_S7_LED_NOT_FOUND;
  for i := Low(LED_NAME) to High(LED_NAME) do
  begin
    if UpperCase(LED_NAME[i]) = UpperCase(aLedName) then
    begin
      Result := i;
      Break;
    end;
  end;
end;

function TS7ExtendedClient.GetLedName(const aLedID: TS7LedIdType): string;
begin
  if GetLedIndex(aLedID) = ERR_S7_LED_NOT_FOUND then
    Result := ''
  else
    Result := LED_NAME[aLedID];
end;

function TS7ExtendedClient.Led(const aLedID: TS7LedIdType; aCpuNum: TS7LedFromCpu = lfCpuMaster): TS7Led;
var
  LedIndex: byte;
  DummyLed: TS7Led;
begin
  if aCpuNum = lfCpuMaster then                     // lfCpuMaster = -1
    aCpuNum := TS7LedFromCpu(Leds.MasterCpuNumber); // change aCpuNum from -1 to 0 or 1

  LedIndex := GetLedIndex(aLedID);

  if LedIndex = ERR_S7_LED_NOT_FOUND then
  begin
    DummyLed.Init;
    Result := DummyLed;
  end
  else
    Result := Leds.Cpu[TS7CpuNum(aCpuNum)].Led[LedIndex];
end;

function TS7ExtendedClient.Led(const aLedName: string; aCpuNum: TS7LedFromCpu = lfCpuMaster): TS7Led;
var
  LedIndex: byte;
  DummyLed: TS7Led;
begin
  if aCpuNum = lfCpuMaster then                     // lfCpuMaster = -1
    aCpuNum := TS7LedFromCpu(Leds.MasterCpuNumber); // change aCpuNum from -1 to 0 or 1

  LedIndex := GetLedIndex(aLedName);

  if LedIndex = ERR_S7_LED_NOT_FOUND then
  begin
    DummyLed.Init;
    Result := DummyLed;
  end
  else
    Result := Leds.Cpu[TS7CpuNum(aCpuNum)].Led[LedIndex];
end;

function TS7ExtendedClient.LedOn(const aLedID: TS7LedIdType; aCpuNum: TS7LedFromCpu = lfCpuMaster): boolean;
begin
  Result := Led(aLedID, aCpuNum).On;
end;

function TS7ExtendedClient.LedOn(const aLedName: string; aCpuNum: TS7LedFromCpu = lfCpuMaster): boolean;
begin
  Result := Led(aLedName, aCpuNum).On;
end;

procedure TS7ExtendedClient.SetSZL(const aId, aIndex: word);
begin
  PDU.H[29] := Hi(aId);
  PDU.H[30] := Lo(aId);
  PDU.H[31] := Hi(aIndex);
  PDU.H[32] := Lo(aIndex);
end;

function TS7ExtendedClient.GetPlcLeds: int;
const // read status of all CPU leds (including redundant S7-400H with CPU and CPU1)
  NOT_YET_SET = $FF; // impossible rack value
var
  i, Index: byte;
  PNumberOfDataRecords: pbyte;
  PLed: plongword;
  SingleLed: TS7Led;
  RackFound, FirstRackFound, SecondRackFound: byte;
  CpuNum: TS7CpuNum;
begin
  LastError       := ErrNone;
  FirstRackFound  := NOT_YET_SET; // impossible rack value by default
  SecondRackFound := NOT_YET_SET; // impossible rack value by default

  InitLeds; // set Found, On and Flashing to false for all leds
  ReadSZL(S7_LEDS_SZL_ID, S7_LEDS_SZL_INDEX); // read all leds from plc

  if LastError = ErrNone then
  begin
    PNumberOfDataRecords := pbyte(@PDU.H[0]) + Shift + 30 + 3; // pointer to byte holding number of data records
    Leds.NumberOfLeds    := PNumberOfDataRecords^; // N_DR

    if Leds.NumberOfLeds > 0 then
    begin // parse leds data
      PLed := plongword(PNumberOfDataRecords + 1);

      for i := 1 to Leds.NumberOfLeds do
      begin
        SingleLed.AsLongword := PLed^;
        Inc(PLed); // point to next 4 bytes
        Index := GetLedIndex(SingleLed.Info.LedID);
        if Index <> ERR_S7_LED_NOT_FOUND then
        begin
          RackFound := SingleLed.Info.Cpu.Rack;

          if (FirstRackFound = NOT_YET_SET) and (SecondRackFound = NOT_YET_SET) then
            FirstRackFound := RackFound;

          if (RackFound <> FirstRackFound) and (SecondRackFound = NOT_YET_SET) then
            SecondRackFound := RackFound;

          if RackFound = FirstRackFound then
            CpuNum := 0
          else
            CpuNum := 1;

          Leds.Redundant                         := SingleLed.Info.Cpu.Redundant;
          Leds.Cpu[CpuNUm].Led[Index].AsLongword := SingleLed.AsLongword;
          Leds.Cpu[CpuNUm].Led[Index].Found      := true;

          if not SingleLed.Info.Cpu.Redundant then
          begin // select CPU 0 as master when found system with only one CPU
            Leds.MasterCpuNumber := 0;
            Leds.MasterCpu.Led   := @Leds.Cpu[0].Led; // point to CPU 0 leds array
            Leds.Cpu[0].Master   := true;
            Leds.Cpu[1].Master   := false;
          end
          else // redundant H-System found
          begin
            if SingleLed.Info.Cpu.Master then
            begin
              Leds.MasterCpuNumber := CpuNum;
              Leds.MasterCpu.Led   := @Leds.Cpu[CpuNum].Led; // point to proper CPU leds array
              if CpuNum = 0 then
              begin
                Leds.Cpu[0].Master   := true;
                Leds.Cpu[1].Master   := false;
              end
              else
              begin
                Leds.Cpu[1].Master   := true;
                Leds.Cpu[0].Master   := false;
              end;
            end;
          end;
        end
        else
          LastError := errS7InvalidPDU;
      end;
    end
    else
      LastError := errS7InvalidPDU;
  end;
  Result := LastError;
end;

function TS7ExtendedClient.GetPlcInfo: int;
const // this is actually cpu info
  SKIP_BYTES                   =  52;
  //
  CPU_INFO_S7_PROJECT_NAME_ID  = $01;
  CPU_INFO_MODULE_NAME_ID      = $02;
  CPU_INFO_COPYRIGHT_ID        = $04;
  CPU_INFO_SERIAL_NUMBER_ID    = $05;
  CPU_INFO_MODULE_TYPE_NAME_ID = $07;
var
  i: int;
  PData: pbyte;
begin
  LastError := ErrNone;

  InitCpuInfo;
  ReadSZL(S7_CPU_INFO_SZL_ID, S7_CPU_INFO_SZL_INDEX); // SZL for cpu info
  Result := LastError;

  if LastError = ErrNone then
  begin
    PData := @PDU.H[0] + SKIP_BYTES; // pointer to byte holding number of data records
    for i := 1 to FReplyMsgLength - SKIP_BYTES do
    begin
      if Pdata^ in [CPU_INFO_S7_PROJECT_NAME_ID, CPU_INFO_MODULE_NAME_ID, CPU_INFO_COPYRIGHT_ID, CPU_INFO_SERIAL_NUMBER_ID, CPU_INFO_MODULE_TYPE_NAME_ID] then
      begin
        case Pdata^ of
          CPU_INFO_S7_PROJECT_NAME_ID:      if (PData + 1)^ in TS7AsciiChars then Move((PData + 1)^, Info.Cpu.ProjectName,    SizeOf(Info.Cpu.ProjectName)); // checking for ascii prevents from overwriting already written project name with zeros
          CPU_INFO_MODULE_NAME_ID:          if (PData + 1)^ in TS7AsciiChars then Move((PData + 1)^, Info.Cpu.ModuleName,     SizeOf(Info.Cpu.ModuleName));
          CPU_INFO_COPYRIGHT_ID:            if (PData + 1)^ in TS7AsciiChars then Move((PData + 1)^, Info.Cpu.Copyright,      SizeOf(Info.Cpu.Copyright));
          CPU_INFO_SERIAL_NUMBER_ID:        if (PData + 1)^ in TS7AsciiChars then Move((PData + 1)^, Info.Cpu.SerialNumber,   SizeOf(Info.Cpu.SerialNumber));
          CPU_INFO_MODULE_TYPE_NAME_ID:     if (PData + 1)^ in TS7AsciiChars then Move((PData + 1)^, Info.Cpu.ModuleTypeName, SizeOf(Info.Cpu.ModuleTypeName));
        end;
      end;
      Inc(PData);
    end;
  end;
end;

function TS7ExtendedClient.GetPlcOrderCode: int;
const // this is actually cpu order code
  SKIP_BYTES = 52;
  ORDER_CODE_ID = $01;
var
  i: int;
  OrderCodeFound: boolean;
  PData: pbyte;
begin
  LastError := ErrNone;

  InitOrderCode;
  ReadSZL(S7_ORDER_CODE_SZL_ID, S7_ORDER_CODE_SZL_INDEX); // SZL for order code
  Result := LastError;

  if LastError = ErrNone then
  begin
    PData := @PDU.H[0] + SKIP_BYTES; // pointer to byte holding number of data records
    OrderCodeFound := false;
    for i := 1 to FReplyMsgLength - SKIP_BYTES do
    begin
      if Pdata^ = ORDER_CODE_ID then
      begin
        OrderCodeFound := true;
        Inc(PData);
        break;
      end;
      Inc(PData);
    end;

    if OrderCodeFound then
      Move(PData^, Info.OrderCode.ProductNumber, SizeOf(Info.OrderCode.ProductNumber)); // Info.OrderCode.ProductNumber := PChar(PData);

    PData := @PDU.H[0] + FReplyMsgLength + 14; // now read cpu version number like v4.0.9
    Info.OrderCode.V1 := (PData + 0)^ ;
    Info.OrderCode.V2 := (Pdata + 1)^;
    Info.OrderCode.V3 := (PData + 2)^;
  end;
end;

function TS7ExtendedClient.GetMemoryType(const aMemType: word): TS7AreaMemoryType;
begin
  case aMemType of
    word(mtRAM):             Result := mtRAM;
    word(mtFEPROM):          Result := mtFEPROM;
    word(mtRAMandFEPROM):    Result := mtRAMandFEPROM;
  else
    {word(mtUnknown):}       Result := mtUnknown;
  end;
end;

function TS7ExtendedClient.GetPlcAreas: int;
const // read size of all CPU system areas I, Q, M, T, C... (s7sfc_en-EN.pdf, Page 708:  SSL-ID W#16#xy14 - System Areas)
  S7_AREA_INPUTS_ID   = $0001; // PII number in bytes
  S7_AREA_OUTPUTS_ID  = $0002; // PIQ (number in bytes)
  S7_AREA_TIMERS_ID   = $0004; // timers (number)
  S7_AREA_COUNTERS_ID = $0005; // counters (number)
  S7_AREA_LOGICAL_ID  = $0006; // number of bytes in the logical address area
  S7_AREA_LOCAL_ID    = $0007; // local data (entire local data area of the CPU in bytes)
  S7_AREA_MEMORY_ID   = $0008; // memory (number in bytes)
var
  i, NumberOfAreas: byte;
  AreaID: word;
  PNumberOfDataRecords: pbyte;
  AreaInfo: TS7AreaInfo;
  PAreaInfo: PS7AreaInfo;
begin
  LastError := ErrNone;
  InitAreas; // initialize all fields
  ReadSZL(S7_SYSTEM_AREAS_SZL_ID, S7_SYSTEM_AREAS_SZL_INDEX); // read size of all system areas from plc

  if LastError = ErrNone then
  begin
    PNumberOfDataRecords := pbyte(@PDU.H[0]) + Shift + 30 + 3; // pointer to byte holding number of data records
    NumberOfAreas        := PNumberOfDataRecords^;             // N_DR

    if NumberOfAreas > 0 then
    begin // parse leds data
      PAreaInfo := PS7AreaInfo(PNumberOfDataRecords + 1);

      for i := 1 to NumberOfAreas do
      begin
        AreaInfo.AsWord[0] := Swap(PAreaInfo^.AsWord[0]);
        AreaInfo.AsWord[1] := Swap(PAreaInfo^.AsWord[1]);
        AreaInfo.AsWord[2] := Swap(PAreaInfo^.AsWord[2]);
        AreaInfo.AsWord[3] := Swap(PAreaInfo^.AsWord[3]);
        AreaID := AreaInfo.AsWord[0];
        case AreaID of
          S7_AREA_INPUTS_ID:     begin    Area.Inputs.Bytes    := AreaInfo.AsWord[2];    Area.Inputs.RetentiveElements   := AreaInfo.AsWord[3];    Area.Inputs.MemoryType   := GetMemoryType(AreaInfo.AsWord[1]);   end;
          S7_AREA_OUTPUTS_ID:    begin    Area.Outputs.Bytes   := AreaInfo.AsWord[2];    Area.Outputs.RetentiveElements  := AreaInfo.AsWord[3];    Area.Outputs.MemoryType  := GetMemoryType(AreaInfo.AsWord[1]);   end;
          S7_AREA_TIMERS_ID:     begin    Area.Timers.Count    := AreaInfo.AsWord[2];    Area.Timers.RetentiveElements   := AreaInfo.AsWord[3];    Area.Timers.MemoryType   := GetMemoryType(AreaInfo.AsWord[1]);   end;
          S7_AREA_COUNTERS_ID:   begin    Area.Counters.Count  := AreaInfo.AsWord[2];    Area.Counters.RetentiveElements := AreaInfo.AsWord[3];    Area.Counters.MemoryType := GetMemoryType(AreaInfo.AsWord[1]);   end;
          S7_AREA_LOGICAL_ID:    begin    Area.Logical.Bytes   := AreaInfo.AsWord[2];    Area.Logical.RetentiveElements  := AreaInfo.AsWord[3];    Area.Logical.MemoryType  := GetMemoryType(AreaInfo.AsWord[1]);   end;
          S7_AREA_LOCAL_ID:      begin    Area.Local.Bytes     := AreaInfo.AsWord[2];    Area.Local.RetentiveElements    := AreaInfo.AsWord[3];    Area.Local.MemoryType    := GetMemoryType(AreaInfo.AsWord[1]);   end;
          S7_AREA_MEMORY_ID:     begin    Area.Memory.Bytes    := AreaInfo.AsWord[2];    Area.Memory.RetentiveElements   := AreaInfo.AsWord[3];    Area.Memory.MemoryType   := GetMemoryType(AreaInfo.AsWord[1]);   end;
        end;
        Inc(PAreaInfo); // point to next 4 words
      end;
    end
    else
      LastError := errS7InvalidPDU;
  end;
  Result := LastError;
end;

function TS7ExtendedClient.GetPlcBatteryStatus: int;
const // this is actually power supply battery status
  SKIP_BYTES       =  52;
  NO_BATTERY_FAULT =   0;
var
  PData: pbyte;
begin
  LastError := ErrNone;

  InitBatteryStatus; // Battery := psbNotSupported;
  ReadSZL(S7_POWER_SUPPLY_BATTERY_FAULT_SZL_ID, S7_POWER_SUPPLY_BATTERY_FAULT_SZL_INDEX); // SZL for power supply battery status

  if LastError = ErrNone then
  begin
    Result := LastError;
    PData := @PDU.H[0] + SKIP_BYTES; // pointer to byte holding number of data records
    if Pdata^ = NO_BATTERY_FAULT then
      Battery := psbGood
    else
      Battery := psbFailure;
  end
  else
    if LastError = errS7InvalidPDU then // S7-300 replied with msg length of 26 (PDU error) because it has no battery
    begin // fix for S7-300 to return psbNotSupported in Battery;
      LastError := ErrNone;
      Result := LastError;
    end;
end;

function TS7ExtendedClient.ReadSZL(const aId, aIndex: word): int;
begin
  LastError := ErrNone;
  Move(S7_MSG_REQ_HEADER, PDU.H, SizeOf(S7_MSG_REQ_HEADER));
  SetSZL(aId, aIndex); // set SZL Id and SZL Index
  PDUH_out.Sequence := GetNextSequence; // autoinc
  if TCPClient.SendBuffer(@PDU.H, SizeOf(S7_MSG_REQ_HEADER)) = SizeOf(S7_MSG_REQ_HEADER) then
  begin
    RecvISOPacket(FReplyMsgLength);
    if LastError = ErrNone then
      if FReplyMsgLength < 50 then  // 50 is the minimum expected reply for GetPlcBatteryStatus (unless plc is without battery when msg len can be even 26), and 54 for everything else
        LastError := errS7InvalidPDU;
  end
  else
    LastError := errTCPDataSend;
  Result := LastError;
end;

function TS7ExtendedClient.GetBlockPointer(const aBlockID: TS7BlockID): PTS7Block;
begin
  case aBlockID of
    blkOB:    Result := @Blocks.OB;
    blkSDB:   Result := @Blocks.SDB;
    blkFC:    Result := @Blocks.FC;
    blkSFC:   Result := @Blocks.SFC;
    blkFB:    Result := @Blocks.FB;
    blkSFB:   Result := @Blocks.SFB;
  else
    {blkDB:}  Result := @Blocks.DB;
  end;
end;

function TS7ExtendedClient.GetPlcBlockList(const aBlockID: TS7BlockID = blkDB): int;
const
  NEXT_PACKET_SEQ_ID_PUT_POS = 24;
  NEXT_PACKET_SEQ_ID_GET_POS = 34;
  BLOCK_ID_POS               = 30;
  ISO_DUMMY_PACKET_SIGNATURE = 751;
var
  Done, First: boolean;
  IsoSize: int;
  SinglePacketBlockCnt, c, Last: word;
  NextPacketSeq: byte;
  PBlock: PTS7Block;
begin
  LastError := ErrNone;

  Done          := false;
  First         := true;
  NextPacketSeq := 0; // first group sequence, next will come from PLC
  Last          := 0;

  PBlock := GetBlockPointer(aBlockID);
  FillChar(PBlock^, SizeOf(PBlock^), 0); // init both PBlock^.List and PBlock^.Count with zeros

  repeat
    if First then
    begin
      First := false;
      Move(S7_MSG_REQ_HEADER_LIST_BLOCKS_OF_FIRST, PDU.H, SizeOf(S7_MSG_REQ_HEADER_LIST_BLOCKS_OF_FIRST));
      IsoSize             := SizeOf(S7_MSG_REQ_HEADER_LIST_BLOCKS_OF_FIRST);
      PDU.H[BLOCK_ID_POS] := byte(aBlockID); // BlockType DB, FB, OB...
    end
    else
    begin
      Move(S7_MSG_REQ_HEADER_LIST_BLOCKS_OF_NEXT, PDU.H, SizeOf(S7_MSG_REQ_HEADER_LIST_BLOCKS_OF_NEXT));
      IsoSize             := SizeOf(S7_MSG_REQ_HEADER_LIST_BLOCKS_OF_NEXT);
    end;
    PDUH_out.Sequence                 := GetNextSequence; // autoinc
    PDU.H[NEXT_PACKET_SEQ_ID_PUT_POS] := NextPacketSeq;

    if TCPClient.SendBuffer(@PDU.H, IsoSize) = IsoSize then
    begin
      RecvISOPacket(FReplyMsgLength);
      if (LastError = ErrNone) and (ResParams.ErrNo = ISO_DUMMY_PACKET_SIGNATURE) then // S7-400 sends one small packet before actual data
        RecvISOPacket(FReplyMsgLength);                                                 // so request actual data read

      if LastError = ErrNone then
      begin
        if FReplyMsgLength < 38 then  // 38 is the minimum expected reply
        begin
          if FReplyMsgLength = 26 then
            LastError := errCliItemNotAvailable
          else
            LastError := errS7InvalidPDU;
        end
        else
        begin
          if ResParams.ErrNo = 0 then
          begin
            if ResData.RetVal = $FF then
            begin
              Done                 := ((ResParams.Rsvd and $FF00) = 0); // Low order byte = $00 => the sequence is done
              NextPacketSeq        := PDU.H[NEXT_PACKET_SEQ_ID_GET_POS]; //ResParams.Seq;  // every next telegram must have this number
              SinglePacketBlockCnt := ((Swap(ResData.DataLen) - 4 ) div 4) + 1; // Partial counter
              for c := 0 to SinglePacketBlockCnt-1 do
              begin
                PBlock^.List[Last] := Swap(ResData.Items[c].BlockNum); { TODO : not thread safe }
                Inc(Last);
                if Last = $8000 then // only $7FFFF items are possible
                begin
                  Done := true;
                  break;
                end
                else
                  PBlock^.Count := Last;
              end;
            end
            else
              Result := errCliItemNotAvailable;
          end
          else
            Result := errCliItemNotAvailable;
        end;
      end
      else
        LastError := errCliItemNotAvailable;
    end
    else
      LastError := errTCPDataSend;
  until not((not Done) and (LastError = ErrNone)); // C++: while (( not Done) and (aResult := 0));

  Result := LastError;
end;

function TS7ExtendedClient.GetPlcAllBlocksList: int;
const
  ERR_UNKNOWN = $4000;
var
  BlockID: TS7BlockID;
  Err: int;
  ResultDetermined: boolean;
begin
  Result := ERR_UNKNOWN;
  ResultDetermined := false;
  for BlockID in S7BlockIDSet do // [blkOB, blkDB, blkSDB, blkFC, blkSFC, blkFB, blkSFB]
  begin
    Err := GetPlcBlockList(BlockID);
    if not ResultDetermined then
      if Err = ErrNone then
      begin
        Result := ErrNone;
        ResultDetermined := true;
      end
      else
        Result := Err;
  end;
end;

function TS7ExtendedClient.GetPlcTime: int;
const // read actual plc date and time
  SKIP_BYTES =  44;
var
  PData: pbyte;
  PlcCentury, PlcDecade, PlcMonth, PlcDay, PlcHour, PlcMinute, PlcSecond: byte;
begin
  LastError := ErrNone;

  InitClock;
  Move(S7_MSG_REQ_HEADER_GET_TIME, PDU.H, SizeOf(S7_MSG_REQ_HEADER_GET_TIME));
  PDUH_out.Sequence := GetNextSequence; // autoinc
  if TCPClient.SendBuffer(@PDU.H, SizeOf(S7_MSG_REQ_HEADER_GET_TIME)) = SizeOf(S7_MSG_REQ_HEADER_GET_TIME) then
  begin
    RecvISOPacket(FReplyMsgLength);
    if LastError = ErrNone then
      if FReplyMsgLength < 32 then  // 36 is the minimum expected reply for GetPlcTime
        LastError := errS7InvalidPDU;
  end
  else
    LastError := errTCPDataSend;

  Result := LastError;

  if LastError = ErrNone then
  begin
    PData      := @PDU.H[0] + SKIP_BYTES;   // pointer to byte holding number of data records
    //PlcCentury := BCDToInt((PData + 0)^); // bad century, since for date 20180810 S7-300 reads 19, S7-400 read 00, and simulator reads 20
    PlcDecade  := BCDToInt((PData + 1)^);
    if PlcDecade >= 90 then                 // fix century
      PlcCentury := 19
    else
      PlcCentury := 20;
    PlcMonth   := BCDToInt((PData + 2)^);
    PlcDay     := BCDToInt((PData + 3)^);
    PlcHour    := BCDToInt((PData + 4)^);
    PlcMinute  := BCDToInt((PData + 5)^);
    PlcSecond  := BCDToInt((PData + 6)^);
    Time := EncodeDateTime(PlcCentury * 100 + PlcDecade, PlcMonth, PlcDay, PlcHour, PlcMinute, PlcSecond, 0); // set S7.Time
  end;
end;

function TS7ExtendedClient.SetPlcTime(const aNewDateTime: TDateTime): int;
const // set plc date and time
  SKIP_BYTES = 30;
var
  Year, Month, Day, Hour, Minute, Second, DummyMillisecond: word;
begin
  LastError := ErrNone;

  InitClock;
  Move(S7_MSG_REQ_HEADER_SET_TIME, PDU.H, SizeOf(S7_MSG_REQ_HEADER_SET_TIME));
  PDUH_out.Sequence := GetNextSequence; // autoinc
  DecodeDateTime(aNewDateTime, Year, Month, Day, Hour, Minute, Second, DummyMillisecond);
  PDURAW.Bytes[SKIP_BYTES + 0] := IntToBCD(Year div 100);                   // $20 from year 2018
  PDURAW.Bytes[SKIP_BYTES + 1] := IntToBCD(Year - (Year div 100) * 100);    // $18 from year 2018
  PDURAW.Bytes[SKIP_BYTES + 2] := IntToBCD(Month);
  PDURAW.Bytes[SKIP_BYTES + 3] := IntToBCD(Day);
  PDURAW.Bytes[SKIP_BYTES + 4] := IntToBCD(Hour);
  PDURAW.Bytes[SKIP_BYTES + 5] := IntToBCD(Minute);
  PDURAW.Bytes[SKIP_BYTES + 6] := IntToBCD(Second);
  if TCPClient.SendBuffer(@PDU.H, SizeOf(S7_MSG_REQ_HEADER_SET_TIME)) = SizeOf(S7_MSG_REQ_HEADER_SET_TIME) then
  begin
    RecvISOPacket(FReplyMsgLength);
    if LastError = ErrNone then
      if FReplyMsgLength < 26 then  // 26 is the minimum expected reply for SetPlcTime
        LastError := errS7InvalidPDU;
  end
  else
    LastError := errTCPDataSend;

  Result := LastError;

  if LastError = ErrNone then
    Time := aNewDateTime; // we can update local plc time variable since plc time change was a success
end;

function TS7ExtendedClient.ErrorToString(aError: int): string;
begin
  case aError of // new errors
    ErrNone                      :  Result := 'No error';
    errNegotiatingPDU            :  Result := 'CPU : Error in PDU negotiation';
    errCliInvalidParams          :  Result := 'CLI : invalid param(s) supplied';
    errCliJobPending             :  Result := 'CLI : Job pending';
    errCliTooManyItems           :  Result := 'CLI : too may items (>20) in multi read/write';
    errCliInvalidWordLen         :  Result := 'CLI : invalid WordLength';
    errCliPartialDataWritten     :  Result := 'CLI : Partial data written';
    errCliSizeOverPDU            :  Result := 'CPU : total data exceeds the PDU size';
    errCliInvalidPlcAnswer       :  Result := 'CLI : invalid CPU answer';
    errCliAddressOutOfRange      :  Result := 'CPU : Address out of range';
    errCliInvalidTransportSize   :  Result := 'CPU : Invalid Transport size';
    errCliWriteDataSizeMismatch  :  Result := 'CPU : Data size mismatch';
    errCliItemNotAvailable       :  Result := 'CPU : Item not available';
    errCliInvalidValue           :  Result := 'CPU : Invalid value supplied';
    errCliCannotStartPLC         :  Result := 'CPU : Cannot start PLC';
    errCliAlreadyRun             :  Result := 'CPU : PLC already RUN';
    errCliCannotStopPLC          :  Result := 'CPU : Cannot stop PLC';
    errCliCannotCopyRamToRom     :  Result := 'CPU : Cannot copy RAM to ROM';
    errCliCannotCompress         :  Result := 'CPU : Cannot compress';
    errCliAlreadyStop            :  Result := 'CPU : PLC already STOP';
    errCliFunNotAvailable        :  Result := 'CPU : Function not available';
    errCliUploadSequenceFailed   :  Result := 'CPU : Upload sequence failed';
    errCliInvalidDataSizeRecvd   :  Result := 'CLI : Invalid data size received';
    errCliInvalidBlockType       :  Result := 'CLI : Invalid block type';
    errCliInvalidBlockNumber     :  Result := 'CLI : Invalid block number';
    errCliInvalidBlockSize       :  Result := 'CLI : Invalid block size';
    errCliDownloadSequenceFailed :  Result := 'CPU : Download sequence failed';
    errCliInsertRefused          :  Result := 'CPU : block insert refused';
    errCliDeleteRefused          :  Result := 'CPU : block delete refused';
    errCliNeedPassword           :  Result := 'CPU : Function not authorized for current protection level';
    errCliInvalidPassword        :  Result := 'CPU : Invalid password';
    errCliNoPasswordToSetOrClear :  Result := 'CPU : No password to set or clear';
    errCliJobTimeout             :  Result := 'CLI : Job Timeout';
    errCliFunctionRefused        :  Result := 'CLI : function refused by CPU (Unknown error)';
    errCliPartialDataRead        :  Result := 'CLI : Partial data read';
    errCliBufferTooSmall         :  Result := 'CLI : The buffer supplied is too small to accomplish the operation';
    errCliDestroying             :  Result := 'CLI : Cannot perform (destroying)';
    errCliInvalidParamNumber     :  Result := 'CLI : Invalid Param Number';
    errCliCannotChangeParam      :  Result := 'CLI : Cannot change this param now';
  else
    if aError < errBufferTooSmall then // tcp, iso, s7 and buffer errors
      Result := inherited ErrorToString(aError) // old s7pas errors
    else
      Result := 'Unknown error';
  end;
end;

end.

