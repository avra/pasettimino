unit uLedDemo;

{$mode delphi}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  s7pas, s7extended, bithelpers;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

{.$define USE-S7-400-PLC-FOR-TESTING}

procedure TForm1.Button1Click(Sender: TObject);
const
  {$ifdef USE-S7-400-PLC-FOR-TESTING}
    // TEST_PLC_IP: TIPAddress = (Bytes: (10, 21, 201, 10)); // PLC1 S7-400H CPU 0 address
    TEST_PLC_IP: TIPAddress = (Bytes: (10, 21, 201, 20));    // PLC6 S7-400H CPU 0 address
    TEST_PLC_SLOT_NUM       =  3;
  {$else}
    TEST_PLC_IP: TIPAddress = (Bytes: (127, 0, 0, 1));    // simulator address
    //TEST_PLC_IP: TIPAddress = (Bytes: (10, 21, 53, 61));  // S7-300 test PLC address
    TEST_PLC_SLOT_NUM       =  2;
  {$endif}
  TEST_PLC_RACK_NUM         =  0;
var
  S7: TS7ExtendedClient;
  MyLed: TS7Led;
  Err, Index: int;
begin
  Memo1.Clear;
  S7 := TS7ExtendedClient.Create;
  try
    //Err := S7.ConnectTo(127, 0, 0, 1, TEST_PLC_RACK_NUM, TEST_PLC_SLOT_NUM);
    Err := S7.ConnectTo(TEST_PLC_IP, TEST_PLC_RACK_NUM, TEST_PLC_SLOT_NUM);
    if Err = ErrNone then
      Memo1.Append('Connected!')
    else
    begin
      Memo1.Append('Connection error. ' + 'Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;

    Err := S7.GetPlcLeds; // get status of all PLC leds (CPU0 and CPU1 leds with redundant S7-400H, and only CPU0 leds with non redundant PLC)
    if Err <> ErrNone then
    begin
      Memo1.Append('GetPlcLeds Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;

    Memo1.Append('Led(S7_LED_RUN_ID).On = '       + S7.Led(S7_LED_RUN_ID).On.ToOneZeroString);
    Memo1.Append('LedOn("STOP").On = '            + S7.LedOn('STOP').ToOneZeroString);
    Memo1.Append('GetLedName(S7_LED_MSTR_ID) = "' + S7.GetLedName(S7_LED_MSTR_ID) + '"');
    MyLed := S7.Led(S7_LED_RUN_ID);           // S7_LED_RUN_ID, S7_LED_RUN_TXT or 'RUN', it doesn't matter
    //MyLed := S7.Leds.Cpu[0].Led[GetLedIndex(S7_LED_MSTR_ID)];
    //MyLed := S7.Leds.MasterCpu.Led[GetLedIndex(S7_LED_MSTR_ID)];
    Memo1.Append('MyLed := S7.Led(S7_LED_RUN_ID);');
    Memo1.Append('MyLed.Found = '                 + MyLed.Found.ToTrueFalseString(scfUpperCase));
    Memo1.Append('MyLed.Info.Cpu.Rack = '         + IntToStr(MyLed.Info.Cpu.Rack));
    Memo1.Append('MyLed.Info.Cpu.Master = '       + MyLed.Info.Cpu.Master.ToTrueFalseString(scfLowerCase));
    Memo1.Append('MyLed.Info.Cpu.Redundant = '    + MyLed.Info.Cpu.Redundant.ToTrueFalseString);
    Memo1.Append('MyLed.Info.LedID = $'           + MyLed.Info.LedID.ToHexString);
    Memo1.Append('MyLed.Led.Status = '            + IntToStr(MyLed.Led.Status) +  '   (S7_LED_STATUS_LED_OFF=0, S7_LED_STATUS_LED_ON=1)');
    Memo1.Append('MyLed.Flash.Status = '          + MyLed.Flash.Status.ToString + '   (S7_FLASH_STATUS_NO_FLASHING=0, S7_FLASH_STATUS_FAST_FLASHING=1, S7_FLASH_STATUS_SLOW_FLASHING=2)');
    Memo1.Append('MyLed.On = '                    + MyLed.On.ToTrueFalseString);
    Memo1.Append('MyLed.Flashing = '              + MyLed.Flashing.ToOnOffString(scfUpperCase));
    Memo1.Append('Leds.Redundant = '              + S7.Leds.Redundant.ToTrueFalseString(scfUnchangedCase{default behavior which can be ommited}));
    if S7.Leds.Redundant then // S7-400H PLC
    begin
      Memo1.Append('Leds.MasterCpuNumber = '      + byte(S7.Leds.MasterCpuNumber).ToString);
      Memo1.Append('Leds.Cpu[0].Master = '        + S7.Leds.Cpu[0].Master.ToString('Главни процесор', 'Споредни процесор') + '   (localized bit status strings)');
      Memo1.Append('Leds.Cpu[1].Master = '        + S7.Leds.Cpu[1].Master.ToString('Главни процесор', 'Споредни процесор') + '   (localized bit status strings)');
      Memo1.Append('Leds.Cpu[0].Led[GetLedIndex(S7_LED_MSTR_ID)].On = ' + S7.Leds.Cpu[0].Led[S7.GetLedIndex(S7_LED_MSTR_ID)].On.ToTrueFalseString);
      Memo1.Append('Leds.Cpu[1].Led[GetLedIndex(S7_LED_MSTR_ID)].On = ' + S7.Leds.Cpu[1].Led[S7.GetLedIndex(S7_LED_MSTR_ID)].On.ToTrueFalseString);

      Memo1.Append('Led(S7_LED_MSTR_ID).On = '              + S7.Led(S7_LED_MSTR_ID).On.ToOneZeroString);
      Memo1.Append('Led(S7_LED_MSTR_ID, lfCpuMaster).On = ' + S7.Led(S7_LED_MSTR_ID, lfCpuMaster {default}).On.ToOneZeroString);
      Memo1.Append('Led(S7_LED_MSTR_ID, lfCpuZero).On = '   + S7.Led(S7_LED_MSTR_ID, lfCpuZero {from CPU0}).On.ToOneZeroString);
      Memo1.Append('Led(S7_LED_MSTR_ID, lfCpuOne).On = '    + S7.Led(S7_LED_MSTR_ID, lfCpuOne {from CPU1}).On.ToOneZeroString);
      Memo1.Append('Leds.MasterCpu.Led[GetLedIndex(S7_LED_MSTR_ID)].On = '    + S7.Leds.MasterCpu.Led[S7.GetLedIndex(S7_LED_MSTR_ID)].On.ToOneZeroString);
    end;
    Memo1.Append('There are ' + IntToStr(S7.Leds.NumberOfLeds) + ' leds in this PLC:');
    for Index in TS7LedIndex do
      if S7.Led(S7.GetLedID(Index)).Found then // does led exist in this plc?
        if not S7.Leds.Redundant then
          Memo1.Append(LED_NAME[Index] + ' = ' + S7.Led(S7.GetLedID(Index)).On.ToOnOffString) // non redundant plc has only Cpu[0] which is mapped to default cpu
        else
          Memo1.Append('CPU0 ' + LED_NAME[Index] + ' = ' + S7.Leds.Cpu[0].Led[Index].On.ToOnOffString + ',   CPU1 ' + LED_NAME[Index] + ' = ' + S7.Leds.Cpu[1].Led[Index].On.ToOnOffString);
  finally
    S7.Destroy;
    Memo1.Append('Disconnected!');
  end;   // Leds[] record structure uses indexes, while Led() functions uses led id or led name
end;     // To convert led id or name to index, use appropirate GetLedIndex() function

end.

