////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                            //
//  Example for using PASETTIMINO and S7EXTENDED and direct communication to Siemens Simatic S7 PLC family    //
//                                                                                                            //
//  Original Settimino for Arduino converted to Free Pascal by Zeljko Avramovic (user avra in Lazarus forum)  //
//                                                                                                            //
//  Original library documentation and license can be found at http://settimino.sourceforge.net               //
//  This source is published under the same license as original library (commercial use is allowed).          //
//  If you need more powerfull heavy weight library then you can take a look at http://snap7.sourceforge.net  //
//                                                                                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

unit udemo;

{$mode delphi}{$H+} // delphi mode needed to use Client.Leds.MasterCpu.Led[] without dereferencing a pointer

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Buttons, StdCtrls, ExtCtrls, Math, TypInfo,
  s7pas, s7extended, bithelpers;

{.$define DO_IT_SMALL}       // uncomment to perform small and fast data access (not really needed for PC)

type

  { TS7TestForm }

  TS7TestForm = class(TForm)
    SetPlcTimeBtn: TButton;
    ReadLedstBtn: TBitBtn;
    StartPlcBtn: TButton;
    StopPlcBtn: TButton;
    WriteBtn: TButton;
    DangerousActionsAllowed: TCheckBox;
    UpperPanel: TPanel;
    ReadDataBtn: TBitBtn;
    LogMemo: TMemo;
    procedure DangerousActionsAllowedClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ReadLedstBtnClick(Sender: TObject);
    procedure ReadDataBtnClick(Sender: TObject);
    procedure SetPlcTimeBtnClick(Sender: TObject);
    procedure StartPlcBtnClick(Sender: TObject);
    procedure StopPlcBtnClick(Sender: TObject);
    procedure WriteBtnClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

{.$define USE-S7-400-PLC-FOR-TESTING} // uncomment to use S7-400 instead of default S7-300 IP address and slot info

const
{$ifdef USE-S7-400-PLC-FOR-TESTING}
  //TEST_PLC_IP: TIPAddress = (Bytes: (10, 21, 201, 10));    // BF1 PLC1 S7-400H CPU 0 address
  TEST_PLC_IP: TIPAddress = (Bytes: (10, 21, 201, 81));    // BF2 PLC6 S7-400H CPU 0 address
  TEST_PLC_SLOT_NUM       =  3;
  TEST_PLC_DB_NUM         = 20;
  TEST_PLC_RACK_NUM       =  0; // can be 0 or 1 in S7-400H PLC family
{$else}
  TEST_PLC_IP: TIPAddress = (Bytes: (127, 0, 0, 1));         // PLC simulator address
  //TEST_PLC_IP: TIPAddress = (Bytes: (10, 21, 53, 39));       // PLC simulator address
  //TEST_PLC_IP: TIPAddress = (Bytes: (10, 21, 53, 61));       // S7-300 test PLC address
  //TEST_PLC_IP: TIPAddress = (Bytes: (192, 168, 44, 131));    // virtual machine PLC simulator for Wireshark
  TEST_PLC_SLOT_NUM       =  2;
  TEST_PLC_DB_NUM         =  1;
  TEST_PLC_RACK_NUM       =  0;
{$endif}

var
  S7TestForm: TS7TestForm;
  DataBuffer: array[0..65535] of byte; // no data will be larger then this
  S7: TS7ExtendedClient;
  Elapsed: QWord;
  WriteInt: int16 = 777; // initial value for writing to plc (incremented after each write)

implementation

{$R *.lfm}

procedure MemoLog(const LogMsg: string);
begin
  S7TestForm.LogMemo.Lines.Add(DateTimeToStr(Now) + '   ' + LogMsg);
end;

procedure CheckError(ErrNo: int);
begin
  MemoLog('Err(' + IntToStr(ErrNo) + ')=' + S7.ErrorToString(ErrNo));
  if (ErrNo and $00FF) <> ErrNone then // Checks if it's a Severe Error => we need to disconnect
  begin
    MemoLog('SEVERE ERROR, disconnecting.');
    S7.Disconnect;
  end;
end;

procedure MarkTime;
begin
  Elapsed := GetTickCount64;
end;

procedure ShowTime;
begin // Calcs the time
  Elapsed := GetTickCount64 - Elapsed;
  MemoLog('Job time ' + IntToStr(Elapsed) + ' ms.');
end;


{ TS7TestForm }

procedure TS7TestForm.ReadDataBtnClick(Sender: TObject);
var
  Err, Size, CpuStatus, i: int;
  Target: pbyte;
  BlockID: TS7BlockID;
  s: string;
begin
  LogMemo.Clear;
  MemoLog('ReadDataBtnClick');

  S7 := TS7ExtendedClient.Create;
  try
    S7.SetConnectionType(PG); // PG {default}, OP, S7_Basic      !!! you need to check first if you have enough free connections of proper type reserved in your cpu

    Err := S7.ConnectTo(TEST_PLC_IP, TEST_PLC_RACK_NUM, TEST_PLC_SLOT_NUM);
    if Err = ErrNone then
      MemoLog('Connected! Length(PDU)=' + IntToStr(S7.GetPDULength))
    else
    begin
      CheckError(Err);
      Exit;
    end;

    Err := S7.GetPlcStatus(CpuStatus); // get running mode of current CPU
    if Err <> ErrNone then
    begin
      MemoLog('GetPlcStatus Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;
    MemoLog('GetPlcStatus = ' + S7.CpuStatusToString(CpuStatus) + ' <' + IntToStr(CpuStatus) + '>');

    Err := S7.GetPlcAllBlocksList;
    if Err = ErrNone then
    begin
      for BlockID in S7BlockIDSet do // [blkOB, blkDB, blkSDB, blkFC, blkSFC, blkFB, blkSFB]
      begin
        MemoLog('Reading block type ' + BlockID.ToString + ' <$' + Byte(BlockID).ToHexString + '>');
        if S7.GetBlockPointer(BlockID).Count <> 0 then
        begin
          s := '';
          for i := 1 to S7.GetBlockPointer(BlockID).Count do
          begin
            s := s + S7.GetBlockPointer(BlockID).List[i-1].ToString;
            if i < S7.GetBlockPointer(BlockID).Count then
               s := s + ', ';
          end;
          MemoLog('S7.Blocks.' + BlockID.ToString + '.Count = ' + S7.GetBlockPointer(BlockID).Count.ToString);
          MemoLog('S7.Blocks.' + BlockID.ToString + '.List = (' + s + ')');
        end;
      end;
    end
    else
      CheckError(Err);
    //Exit;

    {
    MemoLog('TPDU = ' + SizeOf(TPDU).ToString);
    MemoLog('PduPacketInfo.TPKT.Version   = $' + PduPacketInfo.TPKT.Version.ToHexString);
    MemoLog('PduPacketInfo.TPKT.Reserved  = $' + PduPacketInfo.TPKT.Reserved.ToHexString);
    MemoLog('PduPacketInfo.TPKT.HI_Length = $' + PduPacketInfo.TPKT.HI_Lenght.ToHexString);
    MemoLog('PduPacketInfo.TPKT.LO_Length = $' + PduPacketInfo.TPKT.LO_Lenght.ToHexString);
    MemoLog('PduPacketInfo.COTP.HLength   = $' + PduPacketInfo.COTP.HLength.ToHexString);
    MemoLog('PduPacketInfo.COTP.PDUType   = $' + PduPacketInfo.COTP.PDUType.ToHexString);
    MemoLog('PduPacketInfo.COTP.EoT_Num   = $' + PduPacketInfo.COTP.EoT_Num.ToHexString);
    MemoLog('PduPacketInfo.P              = $' + PduPacketInfo.P.ToHexString);
    MemoLog('PduPacketInfo.PDUType        = $' + PduPacketInfo.PDUType.ToHexString);
    MemoLog('PDUH_out.P         ($32)     = $' + PDUH_out.P.ToHexString);
    MemoLog('PDUH_out.PDUType   ($07)     = $' + PDUH_out.PDUType.ToHexString);
    MemoLog('PDUH_out.AB_EX     ($0000)   = $' + PDUH_out.AB_EX.ToHexString);
    MemoLog('PDUH_out.Sequence  (autoinc) = $' + PDUH_out.Sequence.ToHexString);
    MemoLog('if First then:');
    MemoLog('PDUH_out.Parlen    ($$0800)  = $' + PDUH_out.ParLen.ToHexString);  // 8 bytes params
    MemoLog('PDUH_out.Datalen   ($$0600)  = $' + PDUH_out.DataLen.ToHexString); // 6 bytes data
    MemoLog('Sequence = ' + Sequence.ToString);
    MemoLog(LineEnding + Dump(pbyte(@PDU.H[0]), SizeOf(TPDU), 16) + LineEnding);
    }

    Size := S7.GetDBSize(TEST_PLC_DB_NUM); // we could write to first available DB, but writing to fixed DB is less dangerous if user made an unwanted mistake
    if Size = 0 then
    begin
      MemoLog('GetDBSize Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;
    MemoLog('Size(DB' + IntToStr(TEST_PLC_DB_NUM) + ')=' + IntToStr(Size) + ' bytes');

    {$ifdef DO_IT_SMALL}
      Size := Min(Size, 64);
      Target := nil; // use internal buffer (PDU.DATA[])
    {$else}
      Size := Min(Size, SizeOf(DataBuffer));
      Target := @DataBuffer; // use larger buffer
    {$endif}

    MemoLog('Reading ' + IntToStr(Size) + ' bytes from DB' + IntToStr(TEST_PLC_DB_NUM));
    MarkTime; // Get the current tick
    Err := S7.ReadArea(S7AreaDB,        // We are requesting DB access
                       TEST_PLC_DB_NUM, // DB Number = 20
                       0,               // Start from byte 0
                       Size,            // We need "Size" bytes
                       Target);         // Put them into our target (Buffer or PDU)
    if Err = ErrNone then
    begin
      ShowTime;
      MemoLog('DB' + IntToStr(TEST_PLC_DB_NUM) + ' :' + LineEnding + Dump(Target, Size, 16));
      {$ifdef DO_IT_SMALL}
        MemoLog('DB' + IntToStr(TEST_PLC_DB_NUM) + '.INT0=' + IntToStr(IntegerAt(0)));
      {$else}
        MemoLog('DB' + IntToStr(TEST_PLC_DB_NUM) + '.INT0=' + IntToStr(IntegerAt(@DataBuffer, 0)));
      {$endif}
      MemoLog('New readings:');
      MemoLog('ReadInteger(DB20.INT0)=' + IntToStr(S7.ReadInteger(S7AreaDB, 20, 0)));
      MemoLog('ReadLongInteger(DB72.DINT76)=' + IntToStr(S7.ReadLongInt(S7AreaDB, 72, 76)));
      MemoLog('ReadLongWord(DB72.DWORD76)=' + IntToStr(S7.ReadLongWord(S7AreaDB, 72, 76)));
      MemoLog('ReadFloat(DB72.REAL76)=' + FloatToStr(S7.ReadFloat(S7AreaDB, 72, 76)));
      MemoLog('ReadBoolean(MX8.5)=' + BoolToStr(S7.ReadBoolean(S7AreaM, 0, 8, 5), 'TRUE', 'FALSE'));
    end
    else
      CheckError(Err);

    MemoLog('Read PLC areas info:');
    Err := S7.GetPlcAreas; // get info about CPU system areas
    if Err <> ErrNone then
    begin
      MemoLog('GetPlcAreas Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      // for some reason on S7-300 GetPlcAreas() receives bad reply if called after GetPlcInfo(), so moved here and it work
      // MemoLog('message header:' + LineEnding + Dump(pbyte(@S7.FS7MessageRequestHeader), SizeOf(S7.FS7MessageRequestHeader), 16));
      // MemoLog('Received message:' + LineEnding + Dump(pbyte(@PDU.H[0]), 80 + Shift + 30 + 3, 16));
      Exit;
    end;
    //MemoLog('Received message:' + LineEnding + Dump(pbyte(@PDU.H[0]) + Shift + 30 + 3, 80, 16));
    MemoLog(Format('Inputs:    %5d, %s, %d retentive', [S7.Area.Inputs.Bytes,    S7.Area.Inputs.MemoryType.ToString,   S7.Area.Inputs.RetentiveElements]));
    MemoLog(Format('Outputs:   %5d, %s, %d retentive', [S7.Area.Outputs.Bytes,   S7.Area.Outputs.MemoryType.ToString,  S7.Area.Outputs.RetentiveElements]));
    MemoLog(Format('Memory:    %5d, %s, %d retentive', [S7.Area.Memory.Bytes,    S7.Area.Memory.MemoryType.ToString,   S7.Area.Memory.RetentiveElements]));
    MemoLog(Format('Timers:    %5d, %s, %d retentive', [S7.Area.Timers.Count,    S7.Area.Timers.MemoryType.ToString,   S7.Area.Timers.RetentiveElements]));
    MemoLog(Format('Counters:  %5d, %s, %d retentive', [S7.Area.Counters.Count,  S7.Area.Counters.MemoryType.ToString, S7.Area.Counters.RetentiveElements]));
    MemoLog(Format('Logical:   %5d, %s, %d retentive', [S7.Area.Logical.Bytes,   S7.Area.Logical.MemoryType.ToString,  S7.Area.Logical.RetentiveElements]));
    MemoLog(Format('Local:     %5d, %s, %d retentive', [S7.Area.Local.Bytes,     S7.Area.Local.MemoryType.ToString,    S7.Area.Local.RetentiveElements]));

    MemoLog('Read PLC order code:');
    Err := S7.GetPlcOrderCode; // get product number info
    if Err <> ErrNone then
    begin
      MemoLog('GetPlcOrderCode Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;
    //MemoLog('Received message:' + LineEnding + Dump(pbyte(@PDU.H[0]) + Shift + 30 + 3 + 2, 240 - Shift - 30 - 3, 16));
    MemoLog('CPU product number = ' + S7.Info.OrderCode.ProductNumber);
    MemoLog('Firmware revision  = ' + S7.Info.OrderCode.Version);

    MemoLog('Read PLC info:');
    Err := S7.GetPlcInfo; // get info about current CPU
    if Err <> ErrNone then
    begin
      MemoLog('GetPlcInfo Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;
    //MemoLog('Received message:' + LineEnding + Dump(pbyte(@PDU.H[0] + 52), S7.LastReceivedMessageLength - 52, 16));
    MemoLog('Project name       = ' + S7.Info.Cpu.ProjectName);
    MemoLog('Module name        = ' + S7.Info.Cpu.ModuleName);
    MemoLog('Copyright          = ' + S7.Info.Cpu.Copyright);
    MemoLog('Serial number      = ' + S7.Info.Cpu.SerialNumber);
    MemoLog('Module type name   = ' + S7.Info.Cpu.ModuleTypeName);

    MemoLog('Read PLC time:');
    Err := S7.GetPlcTime;
    if Err <> ErrNone then
    begin
      MemoLog('GetPlcTime Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;
    MemoLog('Current PLC time   = ' + DateTimeToStr(S7.Time));
    //MemoLog('Received message:' + LineEnding + Dump(pbyte(@PDU.H[44]), 80, 16));

    MemoLog('Read PLC battery status:');
    Err := S7.GetPlcBatteryStatus;
    if Err <> ErrNone then
    begin
      MemoLog('GetPlcBatteryStatus Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;
    MemoLog('Power supply battery status = ' + S7.Battery.ToString + ' <' + byte(S7.Battery).ToString + '>');
    //MemoLog('Received message:' + LineEnding + Dump(pbyte(@PDU.H[48]), 80, 16));

  finally
    S7.Destroy;
    MemoLog('Disconnected!');
  end;
end;

procedure TS7TestForm.SetPlcTimeBtnClick(Sender: TObject);
var
  Err: int;
begin
  LogMemo.Clear;
  MemoLog('SetPlcTimeBtnClick');
  S7 := TS7ExtendedClient.Create;
  try
    Err := S7.ConnectTo(TEST_PLC_IP, TEST_PLC_RACK_NUM, TEST_PLC_SLOT_NUM);
    if not S7.Connected then
    begin
      MemoLog('Client not connected');
      Exit;
    end;

    MemoLog('Read old PLC time:');
    Err := S7.GetPlcTime;
    if Err <> ErrNone then
    begin
      MemoLog('GetPlcTime Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;
    MemoLog('Old PLC time   = ' + DateTimeToStr(S7.Time));
    //MemoLog('Received message:' + LineEnding + Dump(pbyte(@PDU.H[44]), 80, 16));

    MemoLog('Write new PLC time:');
    Err := S7.SetPlcTime(now);
    if Err <> ErrNone then
    begin
      MemoLog('SetPlcTime Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;
    MemoLog('PLC time set to current pc time.');

    MemoLog('Read new PLC time:');
    Err := S7.GetPlcTime;
    if Err <> ErrNone then
    begin
      MemoLog('GetPlcTime Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;
    MemoLog('New PLC time   = ' + DateTimeToStr(S7.Time));

  finally
    S7.Destroy;
    MemoLog('Disconnected!');
  end;
end;

procedure TS7TestForm.ReadLedstBtnClick(Sender: TObject);
var
  Err, {ByteSize,} PlcStatus: int;
  //Target: pbyte;
  i{, b}: byte;
  MyLed: TS7Led;
begin
  LogMemo.Clear;
  MemoLog('ReadLedsBtnClick');
  S7 := TS7ExtendedClient.Create;
  try
    Err := S7.ConnectTo(TEST_PLC_IP, TEST_PLC_RACK_NUM, TEST_PLC_SLOT_NUM);
    if Err = ErrNone then
      MemoLog('Connected! Length(PDU)=' + IntToStr(S7.GetPDULength))
    else
    begin
      MemoLog('Connection error. ' + 'Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;

    Err := S7.GetPlcStatus(PlcStatus); // get running mode of connected CPU
    if Err <> ErrNone then
    begin
      MemoLog('GetPlcStatus Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;
    MemoLog('GetPlcStatus = ' + S7.CpuStatusToString(PlcStatus) + ' <' + IntToStr(PlcStatus) + '>');

    Err := S7.GetPlcLeds; // get status of all PLC leds (CPU0 and CPU1 leds with redundant S7-400H, and only CPU0 leds with non redundant PLC)
    //MemoLog('Received whole message:' + LineEnding + Dump(pbyte(@PDU.H[0]), 128, 16) + LineEnding);
    //MemoLog('Reply only:' + LineEnding + Dump(pbyte(@PDU.H[0] + Shift + 30 + 3), 64, 16) + LineEnding);
    if Err <> ErrNone then
    begin
      MemoLog('GetPlcLeds Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;

    {// testing BitHelpers
    // b.Clear;
    b := 0;
    b.Bit[0] := true;
    b.Bit[1] := true;
    b.Bit[2] := true;
    //b.Bit[3] := false;
    b.Bit[4] := true;
    //b := %00001111;
    //b := b and (not (Byte(1) shl 3));
    MemoLog(b.ToString + '   ' + b.ToBinString);
    MemoLog('Bit[0] = ' + b.Bit[0].ToOneZeroString);
    MemoLog('Bit[1] = ' + b.Bit[1].ToOneZeroString);
    MemoLog('Bit[2] = ' + b.Bit[2].ToOneZeroString);
    MemoLog('Bit[3] = ' + b.Bit[3].ToOneZeroString);
    MemoLog('Bit[4] = ' + b.Bit[4].ToOneZeroString);
    MemoLog('Bit[5] = ' + b.Bit[5].ToOneZeroString);
    MemoLog('Bit[6] = ' + b.Bit[6].ToOneZeroString);
    MemoLog('Bit[7] = ' + b.Bit[7].ToOneZeroString);}

    MemoLog('Led(S7_LED_RUN_ID).On = '              + S7.Led(S7_LED_RUN_ID).On.ToOneZeroString);
    MemoLog('Led(S7_LED_STOP_ID).On = '             + S7.Led(S7_LED_STOP_ID).On.ToOneZeroString);
    MemoLog('Led("RUN").On = '                      + S7.Led('RUN').On.ToOnOffString(scfLowerCase));
    MemoLog('Led(S7_LED_STOP_TXT).On = '            + S7.Led(S7_LED_STOP_TXT).On.ToOnOffString(scfUpperCase));
    MemoLog('Client.Leds.Redundant = '              + S7.Leds.Redundant.ToTrueFalseString(scfUnchangedCase{default behavior which can be ommited}));
    if S7.Leds.Redundant then // S7-400H PLC
    begin

      MemoLog('Client.GetLedName(S7_LED_MSTR_ID) = "' + S7.GetLedName(S7_LED_MSTR_ID) + '"');
      MemoLog('Client.Leds.MasterCpuNumber = '        + byte(S7.Leds.MasterCpuNumber).ToString);
      MemoLog('Client.Leds.Cpu[0].Master = '          + S7.Leds.Cpu[0].Master.ToString('Главни процесор', 'Споредни процесор') + '   (localized bit status strings)');
      MemoLog('Client.Leds.Cpu[1].Master = '          + S7.Leds.Cpu[1].Master.ToString('Главни процесор', 'Споредни процесор') + '   (localized bit status strings)');
    end;
    MemoLog('');

    {MemoLog('i. Leds.Name[] ($LedID) = Led().On, LedOn(), Leds.MasterCpu.Led[].On, Leds.Cpu[0].Led[].On, Leds.Cpu[1].Led[].On, Leds.MasterCpu.Led[i].Found:');
    for i := Low(LED_ID) to High(LED_ID) do
    begin
      s := i.ToString + '. ' +  S7.Leds.Name[i] + ' ($' + IntToHex(S7.GetLedID(i), 2) + ') = ' + S7.Led(LED_ID[i]).On.ToOneZeroString + ', ';
      s := s + S7.LedOn(S7.GetLedID(i)).ToOneZeroString + ', ';
      s := s + S7.Leds.MasterCpu.Led[i].On.ToOneZeroString + ', ';
      s := s + S7.Leds.Cpu[0].Led[i].On.ToOneZeroString + ', ';
      s := s + S7.Leds.Cpu[1].Led[i].On.ToOneZeroString + ', ';
      s := s + S7.Leds.MasterCpu.Led[i].Found.ToOneZeroString;
      MemoLog(s);
    end;
    MemoLog('');}

    //ByteSize := 4 + S7.Leds.NumberOfLeds * 4; // Length of data record is 2 bytes and number of data records is 2 bytes, so add 4 bytes to (data record bytes * 4)
    //Target := pbyte(@PDU.H[0]) + Shift + 30;  // skip header and jump right to real data
    //MemoLog(Dump(Target, ByteSize));
    MyLed := S7.Led(S7_LED_RUN_TXT); // S7_LED_RUN_ID, S7_LED_RUN_TXT or 'RUN', it doesn't matter (but first is the fastest)
    MemoLog('MyLed := Led(S7_LED_RUN_TXT)   (CPU RUN led data record = $' + IntToHex(MyLed.AsLongword, 8) + ')');
    {$ifdef USE-S7-400-PLC-FOR-TESTING}
      //MyLed := Client.Led(S7_LED_REDF_ID);
    {$else}
      //MyLed := S7.Led(S7_LED_MAINT);
      //MyLed := S7.Leds.Cpu[0].Led[S7_LED_MAINT];
      //MyLed := S7.Leds.MasterCpu.Led[S7_LED_MAINT];
    {$endif}
    MemoLog('MyLed.Found = ' + MyLed.Found.ToTrueFalseString);
    MemoLog('MyLed.AsByte[0] = $' + MyLed.AsByte[0].ToHexString);
    MemoLog('MyLed.AsByte[1] = $' + MyLed.AsByte[1].ToHexString);
    MemoLog('MyLed.AsByte[2] = $' + MyLed.AsByte[2].ToHexString);
    MemoLog('MyLed.AsByte[3] = $' + MyLed.AsByte[3].ToHexString);
    //MemoLog('Size(MyLed) = ' + IntToStr(SizeOf(MyLed)));
    MemoLog('MyLed.Info.AsWord = $'        + IntToHex(MyLed.Info.AsWord, 4));
    MemoLog('MyLed.Info.AsByte[0] = $'     + MyLed.Info.AsByte[0].ToHexString);
    MemoLog('MyLed.Info.AsByte[1] = $'     + MyLed.Info.AsByte[1].ToHexString);
    if S7.Leds.Redundant then // S7-400H PLC
    begin
      MemoLog('MyLed.Info.Cpu.Rack = '       + IntToStr(MyLed.Info.Cpu.Rack));
      MemoLog('MyLed.Info.Cpu.Master = '     + MyLed.Info.Cpu.Master.ToTrueFalseString({scfUpperCase}));
      MemoLog('MyLed.Info.Cpu.Redundant = '  + MyLed.Info.Cpu.Redundant.ToTrueFalseString);
    end;
    MemoLog('MyLed.Info.LedID = $'         + MyLed.Info.LedID.ToHexString);
    MemoLog('MyLed.Led.Status = '          + IntToStr(MyLed.Led.Status) +  '   (S7_LED_STATUS_LED_OFF=0, S7_LED_STATUS_LED_ON=1)');
    MemoLog('MyLed.Flash.Status = '        + MyLed.Flash.Status.ToString + '   (S7_FLASH_STATUS_NO_FLASHING=0, S7_FLASH_STATUS_FAST_FLASHING=1, S7_FLASH_STATUS_SLOW_FLASHING=2)');
    MemoLog('MyLed.On = '                  + MyLed.On.ToTrueFalseString(scfUpperCase));
    MemoLog('MyLed.Flashing = '            + MyLed.Flashing.ToOnOffString(scfLowerCase));
    MemoLog('GetLedIndex(S7_LED_IF_ID) = ' + byte(S7.GetLedIndex(S7_LED_IF_ID)).ToString);

    if S7.Leds.Redundant then // S7-400H PLC
    begin
      MemoLog('');
      MemoLog('Leds.MasterCpuNumber = '    + byte(S7.Leds.MasterCpuNumber).ToString);
      MemoLog('Leds.Cpu[0].Master = '      + S7.Leds.Cpu[0].Master.ToTrueFalseString);
      MemoLog('Leds.Cpu[1].Master = '      + S7.Leds.Cpu[1].Master.ToTrueFalseString);
      MemoLog('Leds.Cpu[0].Led[GetLedIndex(S7_LED_MSTR_ID)].On = ' + S7.Leds.Cpu[0].Led[S7.GetLedIndex(S7_LED_MSTR_ID)].On.ToTrueFalseString);
      MemoLog('Leds.Cpu[1].Led[GetLedIndex(S7_LED_MSTR_ID)].On = ' + S7.Leds.Cpu[1].Led[S7.GetLedIndex(S7_LED_MSTR_ID)].On.ToTrueFalseString);

      MemoLog('Led(S7_LED_MSTR_ID).On = '              + S7.Led(S7_LED_MSTR_ID).On.ToOneZeroString);
      MemoLog('Led(S7_LED_MSTR_ID, lfCpuMaster).On = ' + S7.Led(S7_LED_MSTR_ID, lfCpuMaster {default}).On.ToOneZeroString);
      MemoLog('Led(S7_LED_MSTR_ID, lfCpuZero).On = '   + S7.Led(S7_LED_MSTR_ID, lfCpuZero {from CPU0}).On.ToOneZeroString);
      MemoLog('Led(S7_LED_MSTR_ID, lfCpuOne).On = '    + S7.Led(S7_LED_MSTR_ID, lfCpuOne {from CPU1}).On.ToOneZeroString);
      MemoLog('Leds.MasterCpu.Led[GetLedIndex(S7_LED_MSTR_ID)].On = ' + S7.Leds.MasterCpu.Led[S7.GetLedIndex(S7_LED_MSTR_ID)].On.ToOneZeroString);
    end;

    MemoLog('');
    MemoLog('There are ' + IntToStr(S7.Leds.NumberOfLeds) + ' leds in this PLC:');
    for i in TS7LedIndex do
      if S7.Led(S7.GetLedID(i)).Found then // does led exist in this plc?
        if not S7.Leds.Redundant then
          MemoLog(LED_NAME[i] + ' = ' + S7.Led(S7.GetLedID(i)).On.ToOnOffString) // non redundant plc has only Cpu[0] which is mapped to default cpu
        else // S7-400H has CPU0 and CPU1
          MemoLog('CPU0 ' + LED_NAME[i] + ' = ' + S7.Leds.Cpu[0].Led[i].On.ToOnOffString + ',   CPU1 ' + LED_NAME[i] + ' = ' + S7.Leds.Cpu[1].Led[i].On.ToOnOffString);
  finally
    S7.Destroy;
    MemoLog('Disconnected!');
  end;
end;

procedure TS7TestForm.StartPlcBtnClick(Sender: TObject);
var
  Err, CpuStatus: int;
begin
  LogMemo.Clear;
  MemoLog('StartPlcBtnClick');
  S7 := TS7ExtendedClient.Create;
  try
    Err := S7.ConnectTo(TEST_PLC_IP, TEST_PLC_RACK_NUM, TEST_PLC_SLOT_NUM);
    if not S7.Connected then
    begin
      MemoLog('Client not connected');
      Exit;
    end;

    Err := S7.GetPlcStatus(CpuStatus); // get running mode of current CPU
    if Err <> ErrNone then
    begin
      MemoLog('GetPlcStatus Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;
    MemoLog('GetPlcStatus = ' + S7.CpuStatusToString(CpuStatus) + ' <' + IntToStr(CpuStatus) + '>');

    if (CpuStatus <> S7CpuStatusRun) and (CpuStatus <> S7CpuStatusRun_Redundant) then
    begin
      MemoLog('Starting CPU...');
      S7.PlcStart;
    end
    else
      MemoLog('CPU is already running so there will be no action to start it');

  finally
    S7.Destroy;
    MemoLog('Disconnected!');
  end;
end;

procedure TS7TestForm.StopPlcBtnClick(Sender: TObject);
var
  Err, CpuStatus: int;
begin
  LogMemo.Clear;
  MemoLog('StopPlcBtnClick');
  S7 := TS7ExtendedClient.Create;
  try
    Err := S7.ConnectTo(TEST_PLC_IP, TEST_PLC_RACK_NUM, TEST_PLC_SLOT_NUM);
    if not S7.Connected then
    begin
      MemoLog('Client not connected');
      Exit;
    end;

    Err := S7.GetPlcStatus(CpuStatus); // get running mode of current CPU
    if Err <> ErrNone then
    begin
      MemoLog('GetPlcStatus Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;
    MemoLog('GetPlcStatus = ' + S7.CpuStatusToString(CpuStatus) + ' <' + IntToStr(CpuStatus) + '>');

    if (CpuStatus = S7CpuStatusRun) or (CpuStatus = S7CpuStatusRun_Redundant) then
    begin
      MemoLog('Stopping CPU...');
      S7.PlcStop;
    end
    else
      MemoLog('CPU is not running so there will be no action to stop it');

  finally
    S7.Destroy;
    MemoLog('Disconnected!');
  end;
end;

procedure TS7TestForm.WriteBtnClick(Sender: TObject);
var
  Err, Size, CpuStatus: int;
  Target: pbyte;
begin
  MemoLog('WriteBtnClick');
  S7 := TS7ExtendedClient.Create;
  try
    Err := S7.ConnectTo(TEST_PLC_IP, TEST_PLC_RACK_NUM, TEST_PLC_SLOT_NUM);
    if not S7.Connected then
    begin
      MemoLog('Client not connected');
      Exit;
    end;

    Err := S7.GetPlcStatus(CpuStatus); // get running mode of current CPU
    if Err <> ErrNone then
    begin
      MemoLog('GetPlcStatus Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;
    MemoLog('GetPlcStatus = ' + S7.CpuStatusToString(CpuStatus) + ' <' + IntToStr(CpuStatus) + '>');

    Size := S7.GetDBSize(TEST_PLC_DB_NUM);
    if Size = 0 then
    begin
      MemoLog('GetDBSize Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;
    MemoLog('Size(DB' + IntToStr(TEST_PLC_DB_NUM) + ')=' + IntToStr(Size) + ' bytes');

    {$ifdef DO_IT_SMALL}
      Size := Min(Size, 64);
      Target := nil; // use internal buffer (PDU.DATA[])
    {$else}
      Size := Min(Size, SizeOf(DataBuffer));
      Target := @DataBuffer; // use larger buffer
    {$endif}

    // WARNING !!! Following lines write to TEST_PLC_IP !!!
    MemoLog('Writing ' + IntToStr(WriteInt) + ' to DB' + IntToStr(TEST_PLC_DB_NUM) + '.INT0');
    WriteInt := Swap(Word(WriteInt));
    Err := S7.WriteArea(S7AreaDB, TEST_PLC_DB_NUM, 0, 2, @WriteInt);
    WriteInt := Swap(Word(WriteInt));
    Inc(WriteInt);
    if Err <> ErrNone then
    begin
      MemoLog('WriteArea Err(' + IntToStr(Err) + ')=' + S7.ErrorToString(Err));
      Exit;
    end;

    MemoLog('Reading ' + IntToStr(Size) + ' bytes from DB' + IntToStr(TEST_PLC_DB_NUM));
    // Get the current tick
    MarkTime;
    Err := S7.ReadArea(S7AreaDB,        // We are requesting DB access
                           TEST_PLC_DB_NUM, // DB Number = 20
                           0,               // Start from byte 0
                           Size,            // We need "Size" bytes
                           Target);         // Put them into our target (Buffer or PDU)
    if Err = ErrNone then
    begin
      ShowTime;
      MemoLog(Dump(Target, Size));
      {$ifdef DO_IT_SMALL}
        MemoLog('DB' + IntToStr(TEST_PLC_DB_NUM) + '.INT0=' + IntToStr(IntegerAt(0)));
      {$else}
        MemoLog('DB' + IntToStr(TEST_PLC_DB_NUM) + '.INT0=' + IntToStr(IntegerAt(@DataBuffer, 0)));
      {$endif}
      MemoLog('New readings:');
      MemoLog('ReadInteger(DB20.INT0)=' + IntToStr(S7.ReadInteger(S7AreaDB, 20, 0)));
    end
    else
      CheckError(Err);
  finally
    S7.Destroy;
    MemoLog('Disconnected!');
  end;
end;

procedure TS7TestForm.FormShow(Sender: TObject);
begin
  WriteBtn.Caption := 'Write to DB' + IntToStr(TEST_PLC_DB_NUM) + '.INT0 !!!';
end;

procedure TS7TestForm.DangerousActionsAllowedClick(Sender: TObject);
begin
  if DangerousActionsAllowed.Checked then
  begin
    WriteBtn.Enabled      := true;
    StartPlcBtn.Enabled   := true;
    StopPlcBtn.Enabled    := true;
    SetPlcTimeBtn.Enabled := true;
  end
  else
  begin
    WriteBtn.Enabled      := false;
    StartPlcBtn.Enabled   := false;
    StopPlcBtn.Enabled    := false;
    SetPlcTimeBtn.Enabled := false;
  end;
end;

end.

